package br.com.adscaller.util;

import android.util.Log;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import br.com.adscaller.BuildConfig;

//import com.crashlytics.android.Crashlytics;

public class LogHelper {

    public static void log(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, StringHelper.coalesce(msg, ""));
        } else {
            //Crashlytics.log(tag + ": " + msg);
        }
    }

    public static void log(Throwable e) {
        if (BuildConfig.DEBUG) {
            e.printStackTrace();
        } else {
            if (e instanceof SocketTimeoutException || e instanceof UnknownHostException) {
                //IGNORE
            } else {
                //Crashlytics.logException(e);
            }
        }
    }

}
