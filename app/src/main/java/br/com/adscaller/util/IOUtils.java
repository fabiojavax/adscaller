package br.com.adscaller.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class IOUtils {

    public IOUtils() {
    }

    public static String toString(InputStream in, String charset) throws IOException {
        byte[] bytes = toBytes(in);
        String texto = new String(bytes, charset);
        return texto;
    }

    public static byte[] toBytes(InputStream in) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            byte[] e = new byte[1024];

            int len;
            while((len = in.read(e)) > 0) {
                bos.write(e, 0, len);
            }

            byte[] bytes = bos.toByteArray();
            byte[] var6 = bytes;
            return var6;
        } catch (Exception var14) {
            var14.printStackTrace();
        } finally {
            try {
                bos.close();
                in.close();
            } catch (IOException var13) {
                var13.printStackTrace();
            }

        }

        return null;
    }

    public static void writeToFile(File file, String url) throws Exception {
        URL url1 = new URL(url);
        byte[] ba1 = new byte[1024];
        FileOutputStream fos1 = new FileOutputStream(file);
        InputStream is1 = url1.openStream();

        int baLength;
        while((baLength = is1.read(ba1)) != -1) {
            fos1.write(ba1, 0, baLength);
        }

        fos1.flush();
        fos1.close();
        is1.close();
    }

    public static void correctExif(File file) {

        try {

            Bitmap photoBm = BitmapFactory.decodeFile(file.getPath());

            ExifInterface exif = new ExifInterface(file.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }

            photoBm = Bitmap.createBitmap(photoBm, 0, 0, photoBm.getWidth(), photoBm.getHeight(), matrix, true);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            photoBm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            FileOutputStream fo = new FileOutputStream(file);

            fo.write(bytes.toByteArray());

            fo.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void compressPhoto(File file, int maxWidth, int maxHeight) {
        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;

            Bitmap photoBm = BitmapFactory.decodeFile(file.getPath(), options);

            ExifInterface exif = new ExifInterface(file.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }

            photoBm = Bitmap.createBitmap(photoBm, 0, 0, photoBm.getWidth(), photoBm.getHeight(), matrix, true);

            photoBm = Bitmap.createScaledBitmap(photoBm, maxWidth, maxHeight, true);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            photoBm.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

            FileOutputStream fo = new FileOutputStream(file);

            fo.write(bytes.toByteArray());

            fo.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
