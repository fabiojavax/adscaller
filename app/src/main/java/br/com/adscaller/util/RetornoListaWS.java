package br.com.adscaller.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by fabiolemos on 1/22/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoListaWS<T> {

    private boolean status;

    private String message;

    private List<T> data;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
