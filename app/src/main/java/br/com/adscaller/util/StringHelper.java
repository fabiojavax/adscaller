package br.com.adscaller.util;

import java.text.Normalizer;
import java.text.Normalizer.Form;

public class StringHelper {
    public static final String NEW_LINE = "\n";
    public static final String SPACE = " ";
    public static final String DOT = ".";
    public static final String TELEPHONE_MASK = "(##) ####-#####";
    public static final String TELEPHONE_MASK_SEM_DDD = "####-#####";

    public StringHelper() {
    }

    public static String setTextMask(String text, String mask) {
        String fmt = "";
        int textcnt = 0;
        if(text != null && text.length() != 0) {
            try {
                for(int i = 0; i < mask.length(); ++i) {
                    if(mask.charAt(i) == 35) {
                        fmt = fmt.concat(text.substring(textcnt, textcnt + 1));
                        ++textcnt;
                    } else {
                        fmt = fmt.concat(mask.substring(i, i + 1));
                    }
                }
            } catch (Exception var5) {
                ;
            }

            return fmt;
        } else {
            return null;
        }
    }

    public static boolean isBlank(String valor) {
        return valor == null || valor.trim().length() == 0;
    }

    public static boolean isNotBlank(String valor) {
        return valor != null && valor.trim().length() > 0;
    }

    public static String removeApas(String s) {
        if(s != null && s.length() >= 2) {
            int inicio = 0;
            int fim = s.length();
            if(s.startsWith("\"")) {
                ++inicio;
            }

            if(s.endsWith("\"")) {
                --fim;
            }

            return s.substring(inicio, fim);
        } else {
            return s;
        }
    }

    public static String truncate(String s, int max) {
        return s.length() > max?s.substring(0, max):s;
    }

    public static String defaultEspaco(String s) {
        return s != null && s.length() != 0?s:" ";
    }

    public static String defaultZero(String s) {
        return s != null && s.length() != 0?s:"0";
    }

    public static String retirarAcentos(String texto) {
        if(texto != null) {
            texto = Normalizer.normalize(texto, Form.NFD);
            texto = texto.replaceAll("[^\\p{ASCII}]", "");
        }

        return texto;
    }

    public static String filtraTextoENumero(String texto) {
        if(texto != null) {
            texto = texto.replaceAll("[^A-Za-z0-9]", "");
        }

        return texto;
    }

    public static String maskTelefone(String telefone) {
        if(telefone == null) {
            return telefone;
        } else {
            telefone = telefone.trim();
            if(telefone.length() != 0 && !telefone.startsWith("0800") && !telefone.startsWith("0300")) {
                telefone = telefone.trim().replaceAll("[^0-9]", "").replaceAll("^0*", "");
                return telefone.length() < 10?setTextMask(telefone, "####-#####"):setTextMask(telefone, "(##) ####-#####");
            } else {
                return telefone;
            }
        }
    }

    public static String capitalizeFirstLetter(String original) {
        return original.length() == 0?original:original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public static String mergeSeparator(String separator, String... args) {
        StringBuilder result = new StringBuilder();

        for(int i = 0; i < args.length; ++i) {
            if(!isBlank(args[i])) {
                if(result.length() > 0) {
                    result.append(separator);
                }

                result.append(args[i]);
            }
        }

        return result.toString();
    }

    public static <T> T coalesce(T a, T b) {
        return a != null?a:b;
    }

    public static <T> T coalesce(T a, T b, T c) {
        return a != null?a:(b != null?b:c);
    }

    public static <T> T coalesce(T... a) {
        Object[] var4 = a;
        int var3 = a.length;

        for(int var2 = 0; var2 < var3; ++var2) {
            Object t = var4[var2];
            if(t != null) {
                return null;
            }
        }

        return null;
    }

    public static String removeNaoNumerico(String texto) {
        return texto.replaceAll("[^0-9]", "");
    }

    public static String removeZeroEsquerda(String texto) {
        return texto.replaceAll("^0*", "");
    }

    public static String normalize(String texto) {
        return Normalizer.normalize(texto, Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    public static String capitalizeFirstsLetters(String texto) {
        char[] array = texto.toCharArray();
        array[0] = Character.toUpperCase(array[0]);

        for(int i = 1; i < array.length; ++i) {
            if(Character.isWhitespace(array[i - 1])) {
                array[i] = Character.toUpperCase(array[i]);
            }
        }

        return new String(array);
    }
}
