package br.com.adscaller.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.KeyEvent;

import br.com.adscaller.R;

public class AlertAndroid {

    public static void showMessageDialog(Context context, String message) {

        Builder confirmDialogBuilder = new Builder(context);
        confirmDialogBuilder.setCancelable(false);
        if (message != null) {
            confirmDialogBuilder.setMessage(message);
        }
        confirmDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmDialogBuilder.setTitle(R.string.app_name);

        AlertDialog alert = confirmDialogBuilder.create();

        alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });

        alert.show();

    }

    public static void showMessageDialog(Context context, int message) {

        Builder confirmDialogBuilder = new Builder(context);
        confirmDialogBuilder.setCancelable(false);
        confirmDialogBuilder.setMessage(message);
        confirmDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmDialogBuilder.setTitle(R.string.app_name);

        AlertDialog alert = confirmDialogBuilder.create();

        alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });

        alert.show();

    }

    public static void showMessageDialog(Context context, int message,
                                         DialogInterface.OnClickListener listener) {

        Builder confirmDialogBuilder = new Builder(context);
        confirmDialogBuilder.setCancelable(false);
        confirmDialogBuilder.setMessage(message);
        confirmDialogBuilder.setPositiveButton(R.string.ok, listener);
        confirmDialogBuilder.setTitle(R.string.app_name);

        AlertDialog alert = confirmDialogBuilder.create();

        alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });

        alert.show();

    }

    public static void showMessageDialog(Context context, String title, int message) {

        Builder confirmDialogBuilder = new Builder(context);
        confirmDialogBuilder.setTitle(title);
        confirmDialogBuilder.setCancelable(false);
        confirmDialogBuilder.setMessage(message);
        confirmDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = confirmDialogBuilder.create();

        alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });

        alert.show();

    }

    public static void showMessageDialog(Context context, String title, String message) {

        Builder confirmDialogBuilder = new Builder(context);
        confirmDialogBuilder.setTitle(title);
        confirmDialogBuilder.setCancelable(false);
        confirmDialogBuilder.setMessage(message);
        confirmDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = confirmDialogBuilder.create();

        alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });

        alert.show();

    }


}
