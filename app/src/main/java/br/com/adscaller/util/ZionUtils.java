package br.com.adscaller.util;

import android.graphics.Color;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by fabiolemos on 2/3/18.
 */

public class ZionUtils {

    public static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static Integer getColorAlfa(String color, float alpha) {
        float opacity = 255 * (1 - alpha);
        color = color.replace("#", "#" + Integer.toHexString((int) opacity));
        return Color.parseColor(color);
    }

}
