package br.com.adscaller.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import br.com.adscaller.BuildConfig;

public class RequestHelper {
    
    private final String KEY_LAST_RESPONSE = "last_return";
    private final String KEY_LAST_PARAMS = "last_params";
    private final String KEY_LAST_REQUEST = "last_request";
    public static final long REQUEST_MIN = 1000;
    private final Context context;
    
    public RequestHelper(Context context) {
        this.context = context;
    }
    
    private boolean logParams = true;
    
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    
    /**
     * Parametros já formatados
     * @param tag
     * @param url
     * @param params
     * @return
     * @throws IOException
     */
    public String post(String tag, String url, String params) throws IOException {
        
        LogHelper.log(tag, "Post: " + url);
        LogHelper.log(tag, params);
        String requestReturn = HttpHelper.doPost(url, params, HttpHelper.UTF8);
        if (!BuildConfig.DEBUG) {
            //Crashlytics.setString(KEY_LAST_REQUEST, "p " + url);
            if (logParams) {
                //Crashlytics.setString(KEY_LAST_PARAMS, "p " + params);
            }
        }
        LogHelper.log(tag, requestReturn);
        if (!BuildConfig.DEBUG) {
            //Crashlytics.setString(KEY_LAST_RESPONSE, "p " + requestReturn);
        }
        return requestReturn;
    }
    
    public String post(String tag, String url, Map<String, String> params) throws IOException {
        
        LogHelper.log(tag, "Post: " + url);
        LogHelper.log(tag, params.toString());
        String requestReturn = HttpHelper.doPost(url, params);
        if (!BuildConfig.DEBUG) {
            //Crashlytics.setString(KEY_LAST_REQUEST, "p " + url);
            if (logParams) {
                //Crashlytics.setString(KEY_LAST_PARAMS, "p " + params.toString());
            }
        }
        LogHelper.log(tag, requestReturn);
        if (!BuildConfig.DEBUG) {
            //Crashlytics.setString(KEY_LAST_RESPONSE, "p " + requestReturn);
        }
        return requestReturn;
    }
    
    public String get(String tag, String url, Map<String, String> params) throws IOException {
        
        LogHelper.log(tag, "Get: " + url);
        LogHelper.log(tag, params.toString());
        LogHelper.log(tag, url + "?" + HttpHelper.getQueryString(params));
        if (!BuildConfig.DEBUG) {
            //Crashlytics.setString(KEY_LAST_REQUEST, "g " + url);
            if (logParams) {
                //Crashlytics.setString(KEY_LAST_PARAMS, "g " + HttpHelper.getQueryString(params));
            }
        }
        String requestReturn = HttpHelper.requestHttpGet(url, params);
        if (!BuildConfig.DEBUG) {
            //Crashlytics.setString(KEY_LAST_RESPONSE, "g " + requestReturn);
        }
        LogHelper.log(tag, requestReturn);
        return requestReturn;
    }
    
    /**
     * Método que espera tempo mímino para não dar só uma piscada no carregamento
     * @param ini
     */
    public static void fakeDelay(Date ini) {
        long fim = new Date().getTime();
        long diff = fim - ini.getTime();
        try {
            Thread.sleep(Math.max(REQUEST_MIN - diff, 0));
        } catch (InterruptedException e) {
        }
    }
    
    /**
     * Não loga os parametros. Por exemplo nas requisições que envolvem senha do usuário
     * @param logParams
     */
    public void setLogParams(boolean logParams) {
        this.logParams = logParams;
    }

}
