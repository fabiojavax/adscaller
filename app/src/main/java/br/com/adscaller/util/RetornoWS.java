package br.com.adscaller.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by fabiolemos on 1/22/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoWS<T> {

    private boolean status;

    private String message;

    private T data;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
