package br.com.adscaller.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.adapter.CategoryAdapter;
import br.com.adscaller.adapter.PromotionAdapter;
import br.com.adscaller.storage.BannerDAO;
import br.com.adscaller.storage.BannerPO;
import br.com.adscaller.transferobject.BannerTO;
import br.com.adscaller.transferobject.CategoryTO;
import br.com.adscaller.transferobject.PromotionTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class PromotionFragment extends Fragment implements LocationListener {

    RecyclerView recyclerViewCategory;
    RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;
    private View layoutRetry;

    private PromotionTask promotionTask;
    private CategoryTask categoryTask;
    List<BannerTO> promotionResponse = new ArrayList<>();

    String latitude;
    String longitude;

    List<CategoryTO> categoryResponse = new ArrayList<>();

    TextView textViewEmpty;

    private BroadcastReceiver refreshReceiver;

    public PromotionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_campaign, container, false);

        textViewEmpty = mainView.findViewById(R.id.textview_empty);

        swipeRefreshLayout = mainView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refresh();
                    }
                }
        );

        layoutRetry = mainView.findViewById(R.id.layout_retry);
        layoutRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        recyclerView = mainView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerViewCategory = mainView.findViewById(R.id.recyclerViewCategory);
        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));

        if (categoryTask != null) {
            categoryTask.cancel(true);
        }
        categoryTask = new CategoryTask(getActivity());
        categoryTask.execute();

        refresh();

        refreshReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshRecycler();
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(refreshReceiver,
                new IntentFilter("refresh.recycler.promotion"));

        UserTO userTO = UserPrefs.getUser(getActivity());

        /*
        BannerDAO bannerDAO = new BannerDAO(getActivity());
        List<BannerPO> bannerPOList = bannerDAO.findAll();
        if (!bannerPOList.isEmpty()) {

            List<BannerTO> bannerTOList = new ArrayList<>();
            for (BannerPO bannerPO: bannerPOList) {
                BannerTO bannerTO = bannerPO.getBannerTO();
                if (bannerTO.getPromotion().getSeller().getCategories().contains(userTO.getSelectedCategory())) {
                    bannerTOList.add(bannerTO);
                }
            }

            recyclerView.setAdapter(new PromotionAdapter(getActivity(), bannerTOList));

            swipeRefreshLayout.setVisibility(View.VISIBLE);
            layoutRetry.setVisibility(View.GONE);

        }*/

        return mainView;

    }

    public void onDestroy() {
        super.onDestroy();
        if (refreshReceiver != null) {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(refreshReceiver);
        }
    }

    private void refresh() {
        if (promotionTask != null) {
            promotionTask.cancel(true);
        }
        promotionTask = new PromotionTask(getActivity());
        promotionTask.execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class PromotionTask extends AsyncTask<Void, String, Boolean> {

        Activity activity;

        PromotionTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(true);
            }

            fillLocation();

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                if (StringHelper.isBlank(latitude)) {
                    latitude = "-20.000000";
                }

                if (StringHelper.isBlank(longitude)) {
                    latitude = "-40.000000";
                }

                String url = "http://adscallerapi.azurewebsites.net/api/Banner/getall/" +
                        UserPrefs.getUser(activity).getToken() + "/" + latitude + "/" + longitude;

                LogHelper.log("getPromotions", url);

                Request request = new Request.Builder()
                        .url(url)
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    System.out.println("API RESPONSE: " + apiResponse);

                    if (StringHelper.isNotBlank(apiResponse)) {

                        Type listType = new TypeToken<ArrayList<BannerTO>>(){}.getType();
                        promotionResponse = gson.fromJson(apiResponse, listType);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (!isCancelled() && success) {

                swipeRefreshLayout.setVisibility(View.VISIBLE);
                layoutRetry.setVisibility(View.GONE);

                refreshRecycler();

                BannerDAO bannerDAO = new BannerDAO(activity);
                for (BannerTO bannerTO: promotionResponse) {
                    BannerPO bannerPO = bannerDAO.findById(bannerTO.getBannerId());
                    if (bannerPO != null) {
                        bannerTO.setPrimaryKey(bannerPO.getBannerTO().getPrimaryKey());
                        bannerDAO.update(new BannerPO(bannerTO));
                    } else {
                        bannerDAO.create(new BannerPO(bannerTO));
                    }
                }

            } else {

                swipeRefreshLayout.setVisibility(View.GONE);
                layoutRetry.setVisibility(View.VISIBLE);

            }

        }

    }

    private void refreshRecycler() {

        UserTO userTO = UserPrefs.getUser(getActivity());

        List<BannerTO> bannerTOList = new ArrayList<>();
        for (BannerTO bannerTO: promotionResponse) {
            if (bannerTO.getPromotion().getSeller().getCategories().contains(userTO.getSelectedCategory())) {
                bannerTOList.add(bannerTO);
            }
        }

        if (!bannerTOList.isEmpty()) {
            textViewEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(new PromotionAdapter(getActivity(), bannerTOList));
        } else {
            textViewEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

    }

    class CategoryTask extends AsyncTask<Void, String, Boolean> {

        Activity activity;

        CategoryTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                String url = "http://adscallerapi.azurewebsites.net/api/Category/" +
                        UserPrefs.getUser(activity).getToken();

                LogHelper.log("getCategory", url);

                Request request = new Request.Builder()
                        .url(url)
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        Type listType = new TypeToken<ArrayList<CategoryTO>>(){}.getType();
                        categoryResponse = gson.fromJson(apiResponse, listType);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (!isCancelled()) {

                if (!success || categoryResponse == null) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    recyclerViewCategory.setAdapter(new CategoryAdapter(categoryResponse, getActivity()));

                }

            }

        }

    }

    private void fillLocation() {

        try {

            LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);

            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setPowerRequirement(Criteria.POWER_LOW);

            String provider = mLocationManager.getBestProvider(criteria, true);

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.requestLocationUpdates(provider, 0, 0, this);

            if (mLocationManager != null) {
                Location location = mLocationManager.getLastKnownLocation(provider);
                if (location != null) {
                    String geo = String.format("%f_%f", location.getLatitude(), location.getLongitude());
                    geo = geo.replace(',', '.').replace('_', ',');
                    latitude = geo.split(",")[0];
                    longitude = geo.split(",")[1];
                }
            }

        } catch (Exception e) {}

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}
