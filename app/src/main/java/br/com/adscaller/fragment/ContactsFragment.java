package br.com.adscaller.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.adscaller.BuildConfig;
import br.com.adscaller.R;
import br.com.adscaller.adapter.ContactsAdapter;
import br.com.adscaller.transferobject.ContactTO;

public class ContactsFragment extends Fragment {

    public static final String FRAGMENT_TAG =
            BuildConfig.APPLICATION_ID + ".FRAGMENT_CONTACTS";

    private static final String PARAM_RETURN = "PARAM_RETURN";

    private String paramReturn;


    private OnFragmentInteractionListener mListener;

    RecyclerView recyclerView;
    ContactsAdapter contactsAdapter;

    public ContactsFragment() {
    }

    public static ContactsFragment newInstance(String paramReturn) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_RETURN, paramReturn);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            paramReturn = getArguments().getString(PARAM_RETURN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_contacts, container, false);

        recyclerView = mainView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getContacts();

        setHasOptionsMenu(true);

        return mainView;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchViewMenu = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchViewMenu.getActionView();
        searchView.setQueryHint(getString(R.string.search_contact));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                contactsAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contactsAdapter.filter(newText);
                return true;
            }
        });

        super.onCreateOptionsMenu(menu,inflater);
    }

    private void getContacts() {

        List<ContactTO> list = new ArrayList<>();

        List<String> uniqueNumbers = new ArrayList<>();

        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,null,null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String plainPhone = phoneNumber.replace(" ", "").replace("-", "")
                .replace("+", "");
            if (!uniqueNumbers.contains(plainPhone)) {
                list.add(new ContactTO(name, phoneNumber));
                uniqueNumbers.add(plainPhone);
            }
        }

        phones.close();

        Collections.sort(list, new Comparator<ContactTO>() {
            @Override
            public int compare(ContactTO o1, ContactTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        contactsAdapter = new ContactsAdapter(list, getActivity(), paramReturn);
        recyclerView.setAdapter(contactsAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
