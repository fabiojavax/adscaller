package br.com.adscaller.fragment;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.adscaller.R;
import br.com.adscaller.activity.BannerActivity;
import br.com.adscaller.activity.MainActivity;
import br.com.adscaller.storage.BannerDAO;
import br.com.adscaller.storage.RecentsDAO;
import br.com.adscaller.storage.RecentsPO;
import br.com.adscaller.transferobject.ContactTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.StringHelper;

public class DialFragment extends Fragment {

    private static int PERMISSION_CALL_PHONE = 101;
    private static int REQUEST_CODE_SHOW_BANNER = 998;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    View dial0, dial1, dial2, dial3, dial4, dial5, dial6, dial7, dial8, dial9, dialspecial1, dialspecial2;

    TextView textViewNumber;

    View viewErase;

    private BroadcastReceiver numberReceiver;

    private OnFragmentInteractionListener mListener;

    public DialFragment() {
    }

    public static DialFragment newInstance(String param1, String param2) {
        DialFragment fragment = new DialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_dial, container, false);

        textViewNumber = mainView.findViewById(R.id.textview_number);

        viewErase = mainView.findViewById(R.id.view_erase);
        viewErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = textViewNumber.getText().toString();
                if (number != null && number.trim().length() > 0) {
                    number = number.substring(0, number.length() - 1);
                    textViewNumber.setText(number);
                }
            }
        });

        dial0 = mainView.findViewById(R.id.dial0);
        dial1 = mainView.findViewById(R.id.dial1);
        dial2 = mainView.findViewById(R.id.dial2);
        dial3 = mainView.findViewById(R.id.dial3);
        dial4 = mainView.findViewById(R.id.dial4);
        dial5 = mainView.findViewById(R.id.dial5);
        dial6 = mainView.findViewById(R.id.dial6);
        dial7 = mainView.findViewById(R.id.dial7);
        dial8 = mainView.findViewById(R.id.dial8);
        dial9 = mainView.findViewById(R.id.dial9);

        dialspecial1= mainView.findViewById(R.id.dialspecial1);
        dialspecial2= mainView.findViewById(R.id.dialspecial2);

        setOnClickDial(dial0, "0");
        setOnClickDial(dial1, "1");
        setOnClickDial(dial2, "2");
        setOnClickDial(dial3, "3");
        setOnClickDial(dial4, "4");
        setOnClickDial(dial5, "5");
        setOnClickDial(dial6, "6");
        setOnClickDial(dial7, "7");
        setOnClickDial(dial8, "8");
        setOnClickDial(dial9, "9");

        setOnClickDial(dialspecial1, "*");
        setOnClickDial(dialspecial2, "#");

        numberReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String number = "";

                if (intent.getStringExtra("number") != null) {
                    number = intent.getStringExtra("number");
                    textViewNumber.setText(number);
                } else {
                    number = textViewNumber.getText().toString();
                    ContactTO newContactTO = new ContactTO();
                    newContactTO.setName(number);
                    newContactTO.setPhone(number);
                    new RecentsDAO(context).create(new RecentsPO(newContactTO));
                    Intent refreshFavoritesBroadcast = new Intent("refresh.fragment.recents");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(refreshFavoritesBroadcast);
                }

                if (StringHelper.isNotBlank(number) && number.trim().length() > 2) {

                    /*
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(getActivity(), new String[]{
                                Manifest.permission.CALL_PHONE}, PERMISSION_CALL_PHONE);

                    } else {*/

                        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(callIntent);

                    //}

                } else {

                    AlertAndroid.showMessageDialog(getActivity(), R.string.type_valid_number);

                }

            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(numberReceiver,
                new IntentFilter("fragment.dial.set.number"));

        mainView.findViewById(R.id.view_video_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String number = textViewNumber.getText().toString();

                if (StringHelper.isNotBlank(number) && number.trim().length() > 2) {

                    ContactTO currentContact = new ContactTO(number, number);;

                    new RecentsDAO(getActivity()).create(new RecentsPO(currentContact));
                    Intent refreshFavoritesBroadcast = new Intent("refresh.fragment.recents");
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(refreshFavoritesBroadcast);

                    ((MainActivity)getActivity()).setCurrentContact(currentContact, 2);

                } else {

                    AlertAndroid.showMessageDialog(getActivity(), R.string.type_valid_number);

                }

            }

        });


        return mainView;

    }

    public String getTextNumber() {
        return textViewNumber.getText().toString();
    }
    private void setOnClickDial(View view, final String key) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewNumber.setText(textViewNumber.getText() + key);
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
