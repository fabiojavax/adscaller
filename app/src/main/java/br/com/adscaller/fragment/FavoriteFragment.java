package br.com.adscaller.fragment;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.activity.ContactsActivity;
import br.com.adscaller.adapter.FavoriteAdapter;
import br.com.adscaller.storage.FavoriteDAO;
import br.com.adscaller.storage.FavoritePO;
import br.com.adscaller.transferobject.ContactTO;

public class FavoriteFragment extends Fragment {

    private static int REQUEST_CODE_ADD_FAVORITE = 999;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private FloatingActionButton buttonAdd;

    View emptyLayout;

    RecyclerView recyclerView;
    FavoriteAdapter favoriteAdapter;

    private BroadcastReceiver refreshReceiver;

    public FavoriteFragment() {
    }

    public static FavoriteFragment newInstance(String param1, String param2) {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_favorite, container, false);

        emptyLayout = mainView.findViewById(R.id.empty_layout);

        buttonAdd = mainView.findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContactsActivity.class);
                getActivity().startActivityForResult(intent, REQUEST_CODE_ADD_FAVORITE);
            }
        });

        recyclerView = mainView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getFavorites();

        setHasOptionsMenu(true);

        refreshReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getFavorites();
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(refreshReceiver,
                new IntentFilter("refresh.fragment.favorites"));

        return mainView;

    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

    public void getFavorites() {

        List<ContactTO> contactList = new ArrayList<>();
        List<FavoritePO> list = new FavoriteDAO(getContext()).findAll();
        if (list != null && list.size() > 0) {
            for (FavoritePO favoritePO: list) {
                contactList.add(favoritePO.getContactTO());
            }
        }

        Collections.sort(contactList, new Comparator<ContactTO>() {
            @Override
            public int compare(ContactTO o1, ContactTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        favoriteAdapter = new FavoriteAdapter(contactList, getActivity());
        recyclerView.setAdapter(favoriteAdapter);

        if (contactList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
        }

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchViewMenu = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchViewMenu.getActionView();
        searchView.setQueryHint(getString(R.string.search_favorite));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                favoriteAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                favoriteAdapter.filter(newText);
                return true;
            }
        });

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
