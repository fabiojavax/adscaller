package br.com.adscaller.fragment;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.adapter.RecentsAdapter;
import br.com.adscaller.storage.RecentsDAO;
import br.com.adscaller.storage.RecentsPO;
import br.com.adscaller.transferobject.ContactTO;

public class RecentsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private View emptyLayout;

    RecyclerView recyclerView;
    RecentsAdapter recentsAdapter;

    private BroadcastReceiver refreshReceiver;

    public RecentsFragment() {
    }

    public static RecentsFragment newInstance(String param1, String param2) {
        RecentsFragment fragment = new RecentsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_recents, container, false);

        emptyLayout = mainView.findViewById(R.id.empty_layout);

        recyclerView = mainView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getRecents();

        setHasOptionsMenu(true);

        refreshReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getRecents();
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(refreshReceiver,
                new IntentFilter("refresh.fragment.recents"));

        return mainView;

    }

    private void getRecents() {

        List<ContactTO> contactList = new ArrayList<>();
        List<RecentsPO> list = new RecentsDAO(getContext()).findAll();
        if (list != null && list.size() > 0) {
            for (RecentsPO recentsPO: list) {
                contactList.add(recentsPO.getContactTO());
            }
        }

        Collections.sort(contactList, new Comparator<ContactTO>() {
            @Override
            public int compare(ContactTO o1, ContactTO o2) {
                return o2.getPrimaryKey().compareTo(o1.getPrimaryKey());
            }
        });

        recentsAdapter = new RecentsAdapter(contactList, getActivity());
        recyclerView.setAdapter(recentsAdapter);

        if (contactList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchViewMenu = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchViewMenu.getActionView();
        searchView.setQueryHint(getString(R.string.search_favorite));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                recentsAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                recentsAdapter.filter(newText);
                return true;
            }
        });

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
