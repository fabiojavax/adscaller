package br.com.adscaller.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.transferobject.SmsReceiveResponse;
import br.com.adscaller.transferobject.SmsRequest;
import br.com.adscaller.transferobject.SmsValidateResponse;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.adscaller.Constants.CONTENT_TYPE_JSON;

public class SmsActivity extends AppCompatActivity {

    Activity activity = this;

    View layoutCode;
    View layoutReceive;
    View layoutValidate;

    EditText editTextPhone;
    EditText editTextCode;

    ReceiveTask receiveTask;
    ValidateTask validateTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        layoutCode = findViewById(R.id.layout_code);
        layoutReceive = findViewById(R.id.layout_receive);
        layoutValidate = findViewById(R.id.layout_validate);

        editTextPhone = findViewById(R.id.edittext_phone);
        editTextCode = findViewById(R.id.edittext_code);

        findViewById(R.id.textview_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layoutReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = editTextPhone.getText().toString();
                if (StringHelper.isBlank(phone)) {
                    AlertAndroid.showMessageDialog(activity, R.string.provide_phone_number);
                } else {
                    if (receiveTask != null) {
                        receiveTask.cancel(true);
                    }
                    receiveTask = new ReceiveTask(activity, phone);
                    receiveTask.execute();
                }
            }
        });

        layoutValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = editTextPhone.getText().toString();
                String code = editTextCode.getText().toString();
                Integer smsCode = 0;
                try {
                    smsCode = Integer.parseInt(code);
                } catch (Exception e) {
                    AlertAndroid.showMessageDialog(activity, R.string.provide__valid_code);
                }
                if (StringHelper.isBlank(phone)) {
                    AlertAndroid.showMessageDialog(activity, R.string.provide_phone_number);
                } else {
                    if (validateTask != null) {
                        validateTask.cancel(true);
                    }
                    validateTask = new ValidateTask(activity, phone, smsCode);
                    validateTask.execute();
                }
            }
        });

    }

    class ReceiveTask extends AsyncTask<Void, String, Boolean> {

        ProgressDialog progressDialog;

        Activity activity;

        SmsReceiveResponse smsResponse;

        String login;

        public ReceiveTask(Activity activity, String login) {
            this.login = login;
            this.activity = activity;
        }

            @Override
            protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(activity.getString(R.string.sending_code));
                progressDialog.setCancelable(false);
            }
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(10, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                SmsRequest smsRequest = new SmsRequest();
                smsRequest.setPhoneNumber(login);
                smsRequest.setCode(null);

                RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(smsRequest));
                Request request = new Request.Builder()
                        .url("http://adscallerapi.azurewebsites.net/api/Auth/visitor")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        this.smsResponse = gson.fromJson(apiResponse, SmsReceiveResponse.class);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }

            return false;

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {

                if (!success || smsResponse == null) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    if (!smsResponse.isSuccess()) {

                        AlertAndroid.showMessageDialog(activity, smsResponse.getMensage());

                    } else {

                        layoutCode.setVisibility(View.VISIBLE);
                        layoutReceive.setVisibility(View.GONE);
                        layoutValidate.setVisibility(View.VISIBLE);

                        AlertAndroid.showMessageDialog(activity, R.string.sms_warning);

                    }

                }

            }

        }

    }

    class ValidateTask extends AsyncTask<Void, String, Boolean> {

        ProgressDialog progressDialog;

        Activity activity;

        SmsValidateResponse smsResponse;

        String login;
        Integer code;

        public ValidateTask(Activity activity, String login, Integer code) {
            this.login = login;
            this.code = code;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(activity.getString(R.string.validating_code));
                progressDialog.setCancelable(false);
            }
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(10, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                SmsRequest smsRequest = new SmsRequest();
                smsRequest.setPhoneNumber(login);
                smsRequest.setCode(code);

                RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(smsRequest));
                Request request = new Request.Builder()
                        .url("http://adscallerapi.azurewebsites.net/api/Auth/visitor")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        this.smsResponse = gson.fromJson(apiResponse, SmsValidateResponse.class);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }

            return false;

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {

                if (!success || smsResponse == null) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    if (!smsResponse.isAuthenticated()) {

                        AlertAndroid.showMessageDialog(activity, smsResponse.getMensage());

                    } else {

                        UserTO userTO = new UserTO();
                        userTO.setToken(smsResponse.getAccessToken());
                        userTO.setLogin(smsResponse.getPhoneNumber());
                        userTO.setName(getString(R.string.guest));

                        UserPrefs.setUser(activity, userTO);

                        finish();

                        activity.startActivity(new Intent(activity, MainActivity.class));

                    }

                }

            }

        }

    }


}
