package br.com.adscaller.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.BuildConfig;
import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.storage.BannerDAO;
import br.com.adscaller.storage.BannerPO;
import br.com.adscaller.task.LoginTask;
import br.com.adscaller.transferobject.BannerTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class LoginActivity extends AppCompatActivity {

    private static int PERMISSION_CONTACTS = 101;
    private static int PERMISSION_CALL_PHONE = 102;

    Activity activity = this;

    EditText editTextUser;
    EditText editTextPass;

    LoginTask loginTask;
    PromotionTask promotionTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.view_visitante).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(activity, SmsActivity.class);
                activity.startActivity(i);

            }
        });

        findViewById(R.id.view_recover).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(activity, RecoverPassActivity.class);
                activity.startActivity(i);

            }
        });

        editTextUser = findViewById(R.id.edittext_user);
        editTextPass = findViewById(R.id.edittext_pass);

        if (BuildConfig.DEBUG) {
            editTextUser.setText("jefnazario+ads@gmail.com");
            editTextPass.setText("teste");
        }

        ColorStateList colorStateList = ColorStateList.valueOf(Color.WHITE);
        ViewCompat.setBackgroundTintList(editTextUser, colorStateList);

        View buttonCadastrar = findViewById(R.id.button_cadastrar);
        buttonCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(activity, new String[]{
                            Manifest.permission.READ_CONTACTS}, PERMISSION_CONTACTS);

                    return;

                }

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(activity, new String[]{
                            Manifest.permission.CALL_PHONE}, PERMISSION_CALL_PHONE);

                    return;

                }

                Intent intent = new Intent(activity, CadastroActivity.class);
                startActivity(intent);

            }
        });

        View buttonEntrar = findViewById(R.id.button_entrar);
        buttonEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(activity, new String[]{
                            Manifest.permission.READ_CONTACTS}, PERMISSION_CONTACTS);

                    return;

                }

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(activity, new String[]{
                            Manifest.permission.CALL_PHONE}, PERMISSION_CALL_PHONE);

                    return;

                }

                boolean emailMatch = Patterns.EMAIL_ADDRESS.matcher(editTextUser.getText().toString()).matches();
                boolean phoneMatch = editTextUser.getText().toString().matches("[0-9]{9,11}$");

                if (StringHelper.isBlank(editTextUser.getText().toString())
                        || StringHelper.isBlank(editTextPass.getText().toString())) {

                    AlertAndroid.showMessageDialog(activity, R.string.preencha_todos_campos);

                } else if (!emailMatch && !phoneMatch) {

                    AlertAndroid.showMessageDialog(activity, R.string.valid_email_or_phone);

                } else {

                    String login = editTextUser.getText().toString();
                    String pass = editTextPass.getText().toString();

                    if (loginTask != null) {
                        loginTask.cancel(true);
                    }

                    loginTask = new LoginTask(activity, login, pass);
                    loginTask.execute();

                }

            }
        });

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_CONTACTS);

        }

        if (promotionTask != null) {
            promotionTask.cancel(true);
        }
        promotionTask = new PromotionTask(this);
        promotionTask.execute();

        if (getIntent().getBooleanExtra("fromCadastro", false)) {
            AlertAndroid.showMessageDialog(this, R.string.successful_signin);
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loginTask != null) {
            loginTask.cancel(true);
        }
    }

    class PromotionTask extends AsyncTask<Void, String, Boolean> {

        Activity activity;

        List<BannerTO> promotionResponse = new ArrayList<>();

        PromotionTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                String url = "http://adscallerapi.azurewebsites.net/api/Banner/getall/" +
                        UserPrefs.getUser(activity).getToken() + "/-20.00000/-40.00000";

                LogHelper.log("getPromotions", url);

                Request request = new Request.Builder()
                        .url(url)
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    System.out.println("API RESPONSE: " + apiResponse);

                    if (StringHelper.isNotBlank(apiResponse)) {

                        Type listType = new TypeToken<ArrayList<BannerTO>>(){}.getType();
                        promotionResponse = gson.fromJson(apiResponse, listType);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (!isCancelled() && success) {

                BannerDAO bannerDAO = new BannerDAO(activity);
                if (!promotionResponse.isEmpty()) {
                    for (BannerTO bannerTO : promotionResponse) {
                        BannerPO bannerPO = bannerDAO.findById(bannerTO.getBannerId());
                        if (bannerPO != null) {
                            bannerTO.setPrimaryKey(bannerPO.getBannerTO().getPrimaryKey());
                            bannerDAO.update(new BannerPO(bannerTO));
                        } else {
                            bannerDAO.create(new BannerPO(bannerTO));
                        }
                    }
                }

            }

        }

    }

}
