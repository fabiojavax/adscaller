package br.com.adscaller.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import br.com.adscaller.BuildConfig;
import br.com.adscaller.Constants;
import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.fragment.PromotionFragment;
import br.com.adscaller.fragment.ContactsFragment;
import br.com.adscaller.fragment.FavoriteFragment;
import br.com.adscaller.fragment.DialFragment;
import br.com.adscaller.fragment.RecentsFragment;
import br.com.adscaller.storage.BannerDAO;
import br.com.adscaller.storage.FavoriteDAO;
import br.com.adscaller.storage.FavoritePO;
import br.com.adscaller.storage.RecentsDAO;
import br.com.adscaller.storage.RecentsPO;
import br.com.adscaller.transferobject.ContactTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AWSCognitoUtil;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.IOUtils;
import br.com.adscaller.util.StringHelper;

public class MainActivity extends AppCompatActivity {

    private static int REQUEST_CODE_ADD_FAVORITE = 999;

    private static int REQUEST_CODE_VOICE_CALL = 998;
    private static int REQUEST_CODE_VIDEO_CALL = 997;

    private int PERMISSION_CONTACTS = 101;
    private int PERMISSION_CALL_PHONE = 102;

    private static int PICK_IMAGE_CAMERA = 1;
    private static int PICK_IMAGE_GALLERY = 2;

    private Activity activity = this;

    private final int PAGES = 5;

    private ViewPager viewPager;
    private PagerAdapter pageAdapter;

    View buttonFavorite;
    View buttonRecent;
    View buttonDial;
    View buttonContacts;
    View buttonCampaign;

    ImageView imageViewFavorite;
    ImageView imageViewRecent;
    ImageView imageViewContacts;
    ImageView imageViewCampaign;

    TextView textViewFavorite;
    TextView textViewRecent;
    TextView textViewContacts;
    TextView textViewCampaign;

    FavoriteFragment favoriteFragment;
    RecentsFragment recentsFragment;
    DialFragment dialFragment;
    ContactsFragment contactsFragment;
    PromotionFragment campaignFragment;

    int colorAccent;
    int deactivatedColor;

    private NavigationView nvDrawer;
    DrawerLayout drawerLayout;

    TextView title;

    //S3 OBJECTS
    String photoPath;
    File sourceFile;
    ProgressDialog progressDialog;
    String nomeEnviado;
    private TransferUtility transferUtility;
    ImageView imageViewProfile;

    ContactTO currentContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserTO userTO = UserPrefs.getUser(activity);
        userTO.setSelectedCategory(3);
        UserPrefs.setUser(this, userTO);

        colorAccent = ContextCompat.getColor(activity, R.color.colorAccent);
        deactivatedColor = ContextCompat.getColor(activity, R.color.iconDeactivatedColor);

        nvDrawer = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer_layout);
        setupDrawerContent(nvDrawer);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        View mActionBarView = getLayoutInflater().inflate(R.layout.action_bar, null);

        title = mActionBarView.findViewById(R.id.title);

        actionBar.setCustomView(mActionBarView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        View menu = findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipMenu();
            }
        });

        pageAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(PAGES);
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new ScreenSlidePageListener());

        buttonFavorite = findViewById(R.id.button_favorite);
        buttonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });

        buttonRecent = findViewById(R.id.button_recent);
        buttonRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });

        buttonDial = findViewById(R.id.button_dial);
        ;
        buttonDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() != 2) {
                    viewPager.setCurrentItem(2);
                } else {

                    currentContact = new ContactTO(dialFragment.getTextNumber(), dialFragment.getTextNumber());
                    ;

                    new RecentsDAO(activity).create(new RecentsPO(currentContact));
                    Intent refreshFavoritesBroadcast = new Intent("refresh.fragment.recents");
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(refreshFavoritesBroadcast);

                    if (!new BannerDAO(activity).findAll().isEmpty()) {

                        Intent intent = new Intent(activity, BannerActivity.class);
                        startActivityForResult(intent, REQUEST_CODE_VOICE_CALL);

                    } else {

                        Intent numberDialBroadcast = new Intent("fragment.dial.set.number");
                        numberDialBroadcast.putExtra("number", currentContact.getPhone());
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(numberDialBroadcast);

                    }

                }

            }
        });

        buttonContacts = findViewById(R.id.button_contacts);
        ;
        buttonContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(3);
            }
        });

        buttonCampaign = findViewById(R.id.button_campaign);
        ;
        buttonCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(4);
            }
        });

        imageViewFavorite = findViewById(R.id.imageview_favorite);
        imageViewRecent = findViewById(R.id.imageview_recent);
        imageViewContacts = findViewById(R.id.imageview_contacts);
        imageViewCampaign = findViewById(R.id.imageview_campaign);

        textViewFavorite = findViewById(R.id.textview_favorite);
        textViewRecent = findViewById(R.id.textview_recent);
        textViewContacts = findViewById(R.id.textview_contacts);
        textViewCampaign = findViewById(R.id.textview_campaign);

        viewPager.setCurrentItem(4);

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_CONTACTS}, PERMISSION_CONTACTS);

        } else {

            getContacts();

        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CALL_PHONE}, PERMISSION_CALL_PHONE);

        }

        transferUtility = AWSCognitoUtil.getTransferUtility(this);

        if (StringHelper.isNotBlank(userTO.getAvatar())) {
            Picasso.with(activity).load(userTO.getAvatar()).into(imageViewProfile);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContacts();
            }
            return;
        }

        if (requestCode == PICK_IMAGE_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchCamera();
            }
            return;
        }
        if (requestCode == PICK_IMAGE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchGallery();
            }
            return;
        }

    }

    private void getContacts() {

        List<String> nameList = new ArrayList<>();
        List<String> phoneList = new ArrayList<>();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            nameList.add(name);
            phoneList.add(phoneNumber);
        }
        System.out.println(nameList);
        System.out.println(phoneList);
        phones.close();

    }

    private void setupDrawerContent(NavigationView navigationView) {

        UserTO userTO = UserPrefs.getUser(activity);

        View headerView = navigationView.getHeaderView(0);

        headerView.findViewById(R.id.layout_sair).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder adb = new AlertDialog.Builder(activity);
                adb.setMessage(R.string.mensagem_confirmacao_sair);
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        UserPrefs.clear(activity);
                        finish();

                        Intent intent = new Intent(activity, LoginActivity.class);
                        startActivity(intent);

                    }
                });
                adb.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                adb.show();

            }
        });

        headerView.findViewById(R.id.layout_my_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AlterarDadosActivity.class);
                intent.putExtra("update", true);
                startActivity(intent);

            }
        });

        headerView.findViewById(R.id.layout_my_buys).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(activity, OrderActivity.class));

            }
        });

        headerView.findViewById(R.id.layout_kart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(activity, KartActivity.class));

            }
        });

        headerView.findViewById(R.id.layout_my_cards).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(activity, CardActivity.class));

            }
        });

        ((TextView) headerView.findViewById(R.id.textview_version)).setText("Versão " + BuildConfig.VERSION_NAME);


        imageViewProfile = headerView.findViewById(R.id.imageview_profile);
        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeProfilePicture();
            }
        });

        if (StringHelper.isNotBlank(userTO.getAvatar())) {
            Picasso.with(activity).load(userTO.getAvatar()).into(imageViewProfile);
        }


        updateUserInfo(navigationView);

    }

    private void updateUserInfo(NavigationView navigationView) {
        UserTO userTO = UserPrefs.getUser(this);
        View headerView = navigationView.getHeaderView(0);
        TextView textViewName = headerView.findViewById(R.id.textview_name);
        textViewName.setText(userTO.getName());
        TextView textViewEmail = headerView.findViewById(R.id.textview_email);
        textViewEmail.setText(userTO.getEmail());
    }

    private void flipMenu() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT, true);
        }
    }

    public void setCurrentContact(ContactTO contactTO, int typeCall) {

        viewPager.setCurrentItem(2);

        currentContact = contactTO;


        if (!new BannerDAO(activity).findAll().isEmpty()) {

            Intent intent = new Intent(this, BannerActivity.class);
            if (typeCall == 1) {
                startActivityForResult(intent, REQUEST_CODE_VOICE_CALL);
            } else {
                startActivityForResult(intent, REQUEST_CODE_VIDEO_CALL);
            }

        } else {

            Intent numberDialBroadcast = new Intent("fragment.dial.set.number");
            numberDialBroadcast.putExtra("number", currentContact.getPhone());
            LocalBroadcastManager.getInstance(activity).sendBroadcast(numberDialBroadcast);

        }

    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new FavoriteFragment();
                case 1:
                    return new RecentsFragment();
                case 2:
                    return new DialFragment();
                case 3:
                    return new ContactsFragment();
                case 4:
                    return new PromotionFragment();
                default:
                    return null;
            }

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            switch (position) {
                case 0:
                    favoriteFragment = (FavoriteFragment) createdFragment;
                    break;
                case 1:
                    recentsFragment = (RecentsFragment) createdFragment;
                    break;
                case 2:
                    dialFragment = (DialFragment) createdFragment;
                    break;
                case 3:
                    contactsFragment = (ContactsFragment) createdFragment;
                    break;
                case 4:
                    campaignFragment = (PromotionFragment) createdFragment;
                    break;
            }
            return createdFragment;
        }

        @Override
        public int getCount() {
            return PAGES;
        }


    }

    public class ScreenSlidePageListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    imageViewFavorite.getDrawable().setTint(colorAccent);
                    textViewFavorite.setTextColor(colorAccent);

                    imageViewRecent.getDrawable().setTint(deactivatedColor);
                    textViewRecent.setTextColor(deactivatedColor);

                    imageViewContacts.getDrawable().setTint(deactivatedColor);
                    textViewContacts.setTextColor(deactivatedColor);

                    imageViewCampaign.getDrawable().setTint(deactivatedColor);
                    textViewCampaign.setTextColor(deactivatedColor);

                    title.setText(R.string.favoritos);

                    break;
                case 1:
                    imageViewRecent.getDrawable().setTint(colorAccent);
                    textViewRecent.setTextColor(colorAccent);

                    imageViewFavorite.getDrawable().setTint(deactivatedColor);
                    textViewFavorite.setTextColor(deactivatedColor);

                    imageViewContacts.getDrawable().setTint(deactivatedColor);
                    textViewContacts.setTextColor(deactivatedColor);

                    imageViewCampaign.getDrawable().setTint(deactivatedColor);
                    textViewCampaign.setTextColor(deactivatedColor);

                    title.setText(R.string.recentes);

                    break;
                case 2:
                    imageViewFavorite.getDrawable().setTint(deactivatedColor);
                    imageViewRecent.getDrawable().setTint(deactivatedColor);
                    imageViewContacts.getDrawable().setTint(deactivatedColor);
                    imageViewCampaign.getDrawable().setTint(deactivatedColor);

                    textViewFavorite.setTextColor(deactivatedColor);
                    textViewRecent.setTextColor(deactivatedColor);
                    textViewContacts.setTextColor(deactivatedColor);
                    textViewCampaign.setTextColor(deactivatedColor);

                    title.setText(R.string.app_name);

                    break;
                case 3:
                    imageViewContacts.getDrawable().setTint(colorAccent);
                    textViewContacts.setTextColor(colorAccent);

                    imageViewFavorite.getDrawable().setTint(deactivatedColor);
                    textViewFavorite.setTextColor(deactivatedColor);

                    imageViewRecent.getDrawable().setTint(deactivatedColor);
                    textViewRecent.setTextColor(deactivatedColor);

                    imageViewCampaign.getDrawable().setTint(deactivatedColor);
                    textViewCampaign.setTextColor(deactivatedColor);

                    title.setText(R.string.contacts);

                    break;
                case 4:
                    imageViewCampaign.getDrawable().setTint(colorAccent);
                    textViewCampaign.setTextColor(colorAccent);

                    imageViewFavorite.getDrawable().setTint(deactivatedColor);
                    textViewFavorite.setTextColor(deactivatedColor);

                    imageViewRecent.getDrawable().setTint(deactivatedColor);
                    textViewRecent.setTextColor(deactivatedColor);

                    imageViewContacts.getDrawable().setTint(deactivatedColor);
                    textViewContacts.setTextColor(deactivatedColor);

                    title.setText(R.string.campanhas);

                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

    }


    public void changeProfilePicture() {

        final CharSequence[] options = {getString(R.string.camera),
                getString(R.string.gallery), getString(R.string.cancel)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.CAMERA}, PICK_IMAGE_CAMERA);
                    } else {
                        launchCamera();
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PICK_IMAGE_GALLERY);
                    } else {
                        launchGallery();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    public void launchCamera() {

        photoPath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/adscaller_" + new Date().getTime() + ".jpg";
        File file = new File(photoPath);

        Uri photoUri;
        if (Build.VERSION.SDK_INT >= 24) {
            photoUri = FileProvider.getUriForFile(this, Constants.FILE_PROVIDER, file);

        } else {
            photoUri = Uri.fromFile(file);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        intent.putExtra("outputFormat", "jpg");
        startActivityForResult(intent, PICK_IMAGE_CAMERA);

    }

    public void launchGallery() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_VOICE_CALL) {

            Intent numberDialBroadcast = new Intent("fragment.dial.set.number");
            numberDialBroadcast.putExtra("number", currentContact.getPhone());
            LocalBroadcastManager.getInstance(activity).sendBroadcast(numberDialBroadcast);

        }

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_VIDEO_CALL) {

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.putExtra("videocall", true);
            callIntent.setData(Uri.parse("tel:" + currentContact.getPhone()));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);

        }

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_CAMERA) {

            try {

                Uri photoUri = Uri.fromFile(new File(photoPath));

                CropImageView.CropShape shape = CropImageView.CropShape.OVAL;

                CropImage.activity(photoUri)
                        .setCropShape(shape)
                        .setInitialCropWindowPaddingRatio(0)
                        .setAllowFlipping(false)
                        .setFixAspectRatio(true)
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .setActivityTitle(getString(R.string.cortar_foto))
                        .start(MainActivity.this);

            } catch (Exception e) {

                AlertAndroid.showMessageDialog(MainActivity.this, R.string.camera_error);

            }

        }

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_GALLERY) {

            Uri photoUri = data.getData();

            CropImageView.CropShape shape = CropImageView.CropShape.OVAL;

            CropImage.activity(photoUri)
                    .setCropShape(shape)
                    .setInitialCropWindowPaddingRatio(0)
                    .setAllowFlipping(false)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.OFF)
                    .setActivityTitle(getString(R.string.cortar_foto))
                    .start(MainActivity.this);
        }

        if (resultCode == Activity.RESULT_OK && requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri imageUri = result.getUri();
            sourceFile = new File(imageUri.getPath());
            uploadFile(sourceFile);
        }

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_ADD_FAVORITE) {
            if (data.getSerializableExtra(ContactTO.PARAM) != null) {
                ContactTO contactTO = (ContactTO) data.getSerializableExtra(ContactTO.PARAM);
                new FavoriteDAO(activity).create(new FavoritePO(contactTO));
                favoriteFragment.getFavorites();
            }
        }

    }

    private void uploadFile(File file) {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.wait));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        IOUtils.compressPhoto(sourceFile, 750, 750);

        nomeEnviado = UUID.randomUUID().toString() + new Date().getTime();

        String extensao = "";
        if (file.getName().contains(".")) {
            extensao = file.getName().substring(file.getName().indexOf("."));
        }
        nomeEnviado += extensao;

        TransferObserver observer = transferUtility.upload("youback", nomeEnviado, file);

        observer.setTransferListener(new UploadListener());

    }

    private class UploadListener implements TransferListener {

        @Override
        public void onError(int id, Exception e) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(MainActivity.this);
            }
            builder.setTitle(R.string.app_name)
                    .setMessage(MainActivity.this.getString(R.string.send_fail))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            if (newState.equals(TransferState.COMPLETED)) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                UserTO userTO = UserPrefs.getUser(activity);
                userTO.setAvatar(Constants.AWS_ROOT + nomeEnviado);
                UserPrefs.setUser(activity, userTO);

                Picasso.with(activity).load(userTO.getAvatar()).into(imageViewProfile);

            }

        }


    }

}
