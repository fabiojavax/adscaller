package br.com.adscaller.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.adapter.VoucherAdapter;
import br.com.adscaller.storage.VoucherDAO;
import br.com.adscaller.storage.VoucherPO;
import br.com.adscaller.transferobject.VoucherTO;

public class VoucherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.my_vouchers);

        setContentView(R.layout.activity_voucher);

        List<VoucherTO> voucherTOList = new ArrayList<>();
        List<VoucherPO> voucherPOList = new VoucherDAO(this).findAll();
        for (VoucherPO voucherPO: voucherPOList) {
            voucherTOList.add(voucherPO.getVoucherTO());
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new VoucherAdapter(voucherTOList, this));

    }

}
