package br.com.adscaller.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.adapter.CardAdapter;
import br.com.adscaller.adapter.KartAdapter;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.transferobject.KartTO;

public class KartActivity extends AppCompatActivity {

    private Activity activity = this;

    RecyclerView recyclerView;

    View mainLayout;
    View emptyLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kart);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mainLayout = findViewById(R.id.main_layout);
        emptyLayout = findViewById(R.id.empty_layout);

        List<KartPO> kartPOList = new KartDAO(activity).findAll();

        if (kartPOList.isEmpty()) {

            mainLayout.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);

        } else {

            mainLayout.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);

            List<KartTO> kartTOList = new ArrayList<>();
            for (KartPO kartPO: kartPOList) {
                kartTOList.add(kartPO.getKartTO());
            }

            recyclerView.setAdapter(new KartAdapter(activity, kartTOList));

        }

        findViewById(R.id.button_checkout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, PaymentActivity.class));
            }
        });



    }
}
