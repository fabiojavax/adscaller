package br.com.adscaller.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import br.com.adscaller.R;
import br.com.adscaller.transferobject.RecoverPassRequest;
import br.com.adscaller.transferobject.ResetPassRequest;
import br.com.adscaller.transferobject.ResetPassResponse;
import br.com.adscaller.transferobject.SmsReceiveResponse;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;

import java.util.concurrent.TimeUnit;

import static br.com.adscaller.Constants.CONTENT_TYPE_JSON;

public class RecoverPassActivity extends AppCompatActivity {

    Activity activity = this;

    private View layoutCode;
    private View layoutReceive;
    private View layoutValidate;

    private RadioButton radioSms;
    private RadioButton radioEmail;

    private EditText editTextEmailPhone;
    private EditText editTextCode;
    private EditText editTextPass;

    private ReceiveTask receiveTask;
    private ResetPassTask resetPassTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_pass);

        layoutCode = findViewById(R.id.layout_code);
        layoutReceive = findViewById(R.id.layout_receive);
        layoutValidate = findViewById(R.id.layout_validate);

        editTextEmailPhone = findViewById(R.id.edittext_email_phone);
        editTextPass = findViewById(R.id.edittext_pass);
        editTextCode = findViewById(R.id.edittext_code);

        radioSms = findViewById(R.id.radio_sms);
        radioSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextEmailPhone.setHint(R.string.phone_with_ddd);
            }
        });

        radioEmail = findViewById(R.id.radio_email);
        radioEmail.setChecked(true);
        radioEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextEmailPhone.setHint(R.string.provide_email);
            }
        });

        findViewById(R.id.textview_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layoutReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer type = radioSms.isChecked() ? 0 : 1;

                String phone = editTextEmailPhone.getText().toString();
                if (StringHelper.isBlank(phone)) {
                    AlertAndroid.showMessageDialog(activity, R.string.provide_email_or_phone);
                } else {
                    if (receiveTask != null) {
                        receiveTask.cancel(true);
                    }
                    receiveTask = new ReceiveTask(activity, phone, type);
                    receiveTask.execute();
                }
            }
        });

        layoutValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login = editTextEmailPhone.getText().toString();
                String pass = editTextPass.getText().toString();
                String code = editTextCode.getText().toString();
                Integer smsCode = 0;

                try {

                    smsCode = Integer.parseInt(code);

                    if (StringHelper.isBlank(login)) {
                        AlertAndroid.showMessageDialog(activity, R.string.provide_email_or_phone);
                    } else if (StringHelper.isBlank(pass)) {
                        AlertAndroid.showMessageDialog(activity, R.string.provide_pass);
                    } else {
                        if (resetPassTask != null) {
                            resetPassTask.cancel(true);
                        }
                        resetPassTask = new ResetPassTask(activity, login, pass, smsCode);
                        resetPassTask.execute();
                    }

                } catch (Exception e) {
                    AlertAndroid.showMessageDialog(activity, R.string.provide__valid_code);
                }

            }
        });



    }

    class ReceiveTask extends AsyncTask<Void, String, Boolean> {

        ProgressDialog progressDialog;

        Activity activity;

        SmsReceiveResponse smsResponse;

        String value;
        Integer type;

        public ReceiveTask(Activity activity, String value, Integer type) {
            this.value = value;
            this.type = type;
            this.activity = activity;
        }

            @Override
            protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(activity.getString(R.string.sending_code));
                progressDialog.setCancelable(false);
            }
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                RecoverPassRequest recoverPassRequest = new RecoverPassRequest();
                if (type == 0) {
                    recoverPassRequest.setPhoneNumber(value);
                } else {
                    recoverPassRequest.setEmail(value);
                }

                RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(recoverPassRequest));
                Request request = new Request.Builder()
                        .url("http://adscallerapi.azurewebsites.net/api/Auth/requestresetcodepassword")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        this.smsResponse = gson.fromJson(apiResponse, SmsReceiveResponse.class);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }

            return false;

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {

                if (!success || smsResponse == null) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    if (!smsResponse.isSuccess()) {

                        AlertAndroid.showMessageDialog(activity, smsResponse.getMensage());

                    } else {

                        if (type == 0) {

                            AlertAndroid.showMessageDialog(activity, R.string.code_sent_sms);

                        } else {

                            AlertAndroid.showMessageDialog(activity, R.string.email_sent_code);

                        }

                    }

                }

            }

        }

    }

    class ResetPassTask extends AsyncTask<Void, String, Boolean> {

        ProgressDialog progressDialog;

        Activity activity;

        ResetPassResponse resetPassResponse;

        String login;
        String password;
        Integer code;

        public ResetPassTask(Activity activity, String login, String password, Integer code) {
            this.login = login;
            this.password = password;
            this.code = code;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(activity.getString(R.string.changing_pass));
                progressDialog.setCancelable(false);
            }
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                ResetPassRequest resetPassRequest = new ResetPassRequest();
                resetPassRequest.setLogin(login);
                resetPassRequest.setPassword(password);
                resetPassRequest.setCode(code);

                RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(resetPassRequest));
                Request request = new Request.Builder()
                        .url("http://adscallerapi.azurewebsites.net/api/Auth/resetpassword")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        resetPassResponse = gson.fromJson(apiResponse, ResetPassResponse.class);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }

            return false;

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {

                if (!success || resetPassResponse == null) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    if (!resetPassResponse.isSuccess()) {

                        AlertAndroid.showMessageDialog(activity, resetPassResponse.getMessage());

                    } else {

                        AlertAndroid.showMessageDialog(activity, R.string.successful_pass_change,
                                new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });

                    }

                }

            }

        }

    }


}
