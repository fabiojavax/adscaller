package br.com.adscaller.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.adapter.OrderAdapter;
import br.com.adscaller.transferobject.OrderItemTO;
import br.com.adscaller.transferobject.OrderTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OrderActivity extends AppCompatActivity {

    View emptyLayout;
    RecyclerView recyclerView;
    View progressView;

    OrderTask orderTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.my_buys);

        setContentView(R.layout.activity_order);

        emptyLayout = findViewById(R.id.empty_layout);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressView = findViewById(R.id.progress);

        if (orderTask != null) {
            orderTask.cancel(true);
        }
        orderTask = new OrderTask(this);
        orderTask.execute();


    }

    class OrderTask extends AsyncTask<Void, String, Boolean> {

        Activity activity;

        List<OrderTO> orderResponse = new ArrayList<>();

        public OrderTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                String url = "http://adscallerapi.azurewebsites.net/api/Order/myorders/" +
                        UserPrefs.getUser(activity).getToken();

                LogHelper.log("getBuys", url);

                Request request= new Request.Builder()
                        .url(url)
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        Type listType = new TypeToken<ArrayList<OrderTO>>(){}.getType();
                        orderResponse = gson.fromJson(apiResponse, listType);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {

            progressView.setVisibility(View.GONE);

            if (!isCancelled()) {

                if (!success) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    if (orderResponse.isEmpty()) {

                        emptyLayout.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);

                    } else {

                        for (OrderTO orderTO: orderResponse) {
                            StringBuilder itemsDescription = new StringBuilder();
                            List<OrderItemTO> orderItems = orderTO.getOrderItems();
                            if (orderItems != null) {
                                for (OrderItemTO orderItemTO: orderItems) {
                                    itemsDescription.append(orderItemTO.getTitle()).append("\n\n");
                                }
                            }
                            orderTO.setItemsDescription(itemsDescription.toString());
                        }

                        emptyLayout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        recyclerView.setAdapter(new OrderAdapter(orderResponse, activity));

                    }

                }

            }

        }

    }

}
