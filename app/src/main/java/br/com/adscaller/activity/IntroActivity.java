package br.com.adscaller.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import br.com.adscaller.R;
import br.com.adscaller.fragment.Intro1Fragment;
import br.com.adscaller.fragment.Intro2Fragment;
import br.com.adscaller.fragment.Intro3Fragment;

public class IntroActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private PagerAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        pageAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(pageAdapter);

    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new Intro1Fragment();
                case 1:
                    return new Intro2Fragment();
                case 2:
                    return new Intro3Fragment();
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 3;
        }

    }

}
