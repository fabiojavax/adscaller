package br.com.adscaller.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.adscaller.util.StringHelper;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.transferobject.BannerTO;
import br.com.adscaller.transferobject.KartTO;
import br.com.adscaller.util.AlertAndroid;

public class PromotionActivity extends AppCompatActivity {

    Activity activity = this;

    BannerTO bannerTO;

    ImageView imageView;
    TextView textViewDescription;

    TextView textViewSellerName;
    TextView textViewPrice;
    TextView textViewPriceDiscount;

    View viewDecrement;
    TextView textViewQuantity;
    View viewIncrement;

    private BroadcastReceiver closeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_promotion);

        bannerTO = (BannerTO) getIntent().getSerializableExtra("bannerTO");

        imageView = findViewById(R.id.imageview);
        textViewDescription = findViewById(R.id.textview_description);

        textViewSellerName = findViewById(R.id.textview_seller_name);
        textViewPrice = findViewById(R.id.textview_price);
        textViewPriceDiscount = findViewById(R.id.textview_price_discount);

        textViewPrice.setPaintFlags(textViewPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        textViewSellerName.setText(bannerTO.getPromotion().getSeller().getName());

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());

        Double price = bannerTO.getPromotion().getPrice() != null ?
                bannerTO.getPromotion().getPrice() : 0;
        textViewPrice.setText(format.format(price));

        if (bannerTO.getPromotion().getPercentualDiscount() != null &&
                bannerTO.getPromotion().getPercentualDiscount() > 0) {

            Integer percentualDiscount = bannerTO.getPromotion().getPercentualDiscount();

            Double priceDiscount = price - ( (price / 100) * percentualDiscount);

            textViewPriceDiscount.setText(format.format(priceDiscount));

        } else {

            textViewPriceDiscount.setText(format.format(price));

        }

        textViewQuantity = findViewById(R.id.textview_quantity);
        Integer promotionId = bannerTO.getPromotion().getPromotionId();
        KartDAO kartDAO = new KartDAO(activity);
        KartPO existingKart = kartDAO.findByPromotion(promotionId);
        if (existingKart != null) {
            textViewQuantity.setText(String.valueOf(existingKart.getKartTO().getQuantity()));
        }

        viewDecrement = findViewById(R.id.view_decrement);
        viewDecrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer quantity = Integer.parseInt(textViewQuantity.getText().toString());
                if (quantity > 0) {
                    textViewQuantity.setText(String.valueOf(--quantity));

                    KartDAO kartDAO = new KartDAO(activity);

                    Integer promotionId = bannerTO.getPromotion().getPromotionId();

                    KartPO existingKart = kartDAO.findByPromotion(promotionId);

                    if (quantity == 0) {

                        kartDAO.remove(existingKart);

                    } else {

                        existingKart.getKartTO().setQuantity(existingKart.getKartTO().getQuantity() - 1);
                        kartDAO.update(existingKart);

                    }

                }
            }
        });

        viewIncrement = findViewById(R.id.view_increment);
        viewIncrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer quantity = Integer.parseInt(textViewQuantity.getText().toString());
                if (quantity < 99) {

                    textViewQuantity.setText(String.valueOf(++quantity));

                    KartDAO kartDAO = new KartDAO(activity);

                    Integer promotionId = bannerTO.getPromotion().getPromotionId();

                    KartPO existingKart = kartDAO.findByPromotion(promotionId);

                    if (existingKart == null) {

                        KartTO kartTO = new KartTO();
                        kartTO.setPromotionId(promotionId);
                        kartTO.setQuantity(1);
                        kartTO.setBannerUrl(bannerTO.getBannerUrl());
                        kartTO.setSellerName(bannerTO.getPromotion().getSeller().getName());
                        kartTO.setDescription(bannerTO.getPromotion().getDescription());
                        kartTO.setPrice(bannerTO.getPromotion().getPrice());
                        if (bannerTO.getPromotion().getPercentualDiscount() == null ||
                                bannerTO.getPromotion().getPercentualDiscount() == 0) {
                            kartTO.setPriceDiscount(kartTO.getPrice());
                        } else {
                            Integer percentualDiscount = bannerTO.getPromotion().getPercentualDiscount();
                            Double priceDiscount = kartTO.getPrice() - ( (kartTO.getPrice() / 100) * percentualDiscount);
                            kartTO.setPriceDiscount(priceDiscount);
                        }

                        kartTO.setPercentualDiscount(bannerTO.getPromotion().getPercentualDiscount());
                        kartDAO.create(new KartPO(kartTO));

                    } else {

                        existingKart.getKartTO().setQuantity(existingKart.getKartTO().getQuantity() + 1);
                        kartDAO.update(existingKart);

                    }
                }
            }
        });

        textViewDescription.setText(bannerTO.getPromotion().getDescription());
        Picasso.with(this).load(bannerTO.getBannerUrl()).into(imageView);

        findViewById(R.id.button_cadastrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.button_finalize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(textViewQuantity.getText().toString());

                if (quantity > 0) {

                    startActivity(new Intent(activity, PaymentActivity.class));

                } else {

                    AlertAndroid.showMessageDialog(activity, R.string.no_itens_kart);

                }
            }
        });

        closeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        LocalBroadcastManager.getInstance(activity).registerReceiver(closeReceiver,
                new IntentFilter("close.activity.promotion"));

        findViewById(R.id.textview_politica).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertAndroid.showMessageDialog(activity, R.string.pagerme_rules);
            }
        });

        if (StringHelper.isNotBlank(bannerTO.getPromotion().getObs())) {
            findViewById(R.id.textview_regulamento).setVisibility(View.VISIBLE);
            findViewById(R.id.textview_regulamento).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertAndroid.showMessageDialog(activity, bannerTO.getPromotion().getObs());
                }
            });
        } else {
            findViewById(R.id.textview_regulamento).setVisibility(View.GONE);
        }



    }

    public void onDestroy() {
        super.onDestroy();
        if (closeReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(closeReceiver);
        }
    }


}
