package br.com.adscaller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.adapter.CardAdapter;
import br.com.adscaller.adapter.PaymentAdapter;
import br.com.adscaller.storage.CardDAO;
import br.com.adscaller.storage.CardPO;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.task.BuyTask;
import br.com.adscaller.transferobject.CardTO;
import br.com.adscaller.transferobject.KartTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AlertAndroid;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class PaymentActivity extends AppCompatActivity {

    private static int REQUEST_CODE_SCAN_CARD = 1;

    Activity activity = this;

    BuyTask buyTask;

    RecyclerView recyclerViewKart;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        List<KartPO> kartPOList = new KartDAO(activity).findAll();
        List<KartTO> kartTOList = new ArrayList<>();
        for (KartPO kartPO: kartPOList) {
            KartTO kartTO = kartPO.getKartTO();
            kartTOList.add(kartTO);
        }

        recyclerViewKart = findViewById(R.id.recyclerViewKart);
        recyclerViewKart.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewKart.setAdapter(new PaymentAdapter(kartTOList, activity));

        findViewById(R.id.textview_add_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scanIntent = new Intent(activity, CardIOActivity.class);

                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true);
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true);

                startActivityForResult(scanIntent, REQUEST_CODE_SCAN_CARD);
            }
        });

        findViewById(R.id.button_finalize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CardDAO cardDAO = new CardDAO(activity);
                List<CardPO> listCardPO = cardDAO.findAll();
                if (listCardPO.isEmpty()) {
                    AlertAndroid.showMessageDialog(activity, R.string.add_card_to_complete);
                    return;
                }

                AlertDialog.Builder adb = new AlertDialog.Builder(activity);
                adb.setMessage(R.string.buy_confirmation);
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (buyTask != null) {
                            buyTask.cancel(true);
                        }
                        buyTask = new BuyTask(activity);
                        buyTask.execute();

                    } });
                adb.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    } });
                adb.show();



            }
        });

        updateTotal();

        updateCardInfo();

    }

    public void updateTotal() {

        Double total = 0.0;

        List<KartPO> kartPOList = new KartDAO(activity).findAll();
        for (KartPO kartPO: kartPOList) {
            KartTO kartTO = kartPO.getKartTO();
            total += kartTO.getPriceDiscount() * kartTO.getQuantity();
        }

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        TextView textViewTotal = findViewById(R.id.textview_total);
        textViewTotal.setText(format.format(total));

    }

    private void updateCardInfo() {

        CardDAO cardDAO = new CardDAO(activity);
        List<CardPO> listCardPO = cardDAO.findAll();
        if (listCardPO.isEmpty()) {

            findViewById(R.id.layout_empty_card).setVisibility(View.VISIBLE);
            findViewById(R.id.recyclerView).setVisibility(View.GONE);

        } else {

            findViewById(R.id.layout_empty_card).setVisibility(View.GONE);
            findViewById(R.id.recyclerView).setVisibility(View.VISIBLE);

            List<CardTO> cardTOList = new ArrayList<>();
            List<CardPO> cardPOList = new CardDAO(this).findAll();
            for (CardPO cardPO: cardPOList) {
                cardTOList.add(cardPO.getCardTO());
            }

            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new CardAdapter(cardTOList, this));

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SCAN_CARD) {

            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {

                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                CardTO cardTO = new CardTO();
                cardTO.setUserId(UserPrefs.getUser(activity).getId());
                cardTO.setCardNumber(scanResult.cardNumber);
                cardTO.setRedactedCardNumber(scanResult.getRedactedCardNumber());
                cardTO.setCardholderName(scanResult.cardholderName);
                cardTO.setCvv(scanResult.cvv);
                if (scanResult.expiryMonth < 10) {
                    cardTO.setExpiryMonth("0" + scanResult.expiryMonth);
                } else {
                    cardTO.setExpiryMonth(String.valueOf(scanResult.expiryMonth));
                }
                CardDAO cardDAO = new CardDAO(activity);
                CardPO existingCard = cardDAO.findByNumber(cardTO.getCardNumber());
                if (existingCard != null) {
                    AlertAndroid.showMessageDialog(activity, R.string.card_already_added);
                } else {
                    cardDAO.create(new CardPO(cardTO));
                }

                List<CardPO> allCards = cardDAO.findAll();

                if (allCards.size() == 1) {
                    UserTO userTO = UserPrefs.getUser(activity);
                    userTO.setSelectedCard(allCards.get(0).getCardTO().getPrimaryKey());
                    UserPrefs.setUser(activity, userTO);
                }

                updateCardInfo();

            }
        }
    }

}
