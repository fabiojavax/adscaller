package br.com.adscaller.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.adapter.CardAdapter;
import br.com.adscaller.adapter.VoucherAdapter;
import br.com.adscaller.storage.CardDAO;
import br.com.adscaller.storage.CardPO;
import br.com.adscaller.storage.VoucherDAO;
import br.com.adscaller.storage.VoucherPO;
import br.com.adscaller.transferobject.CardTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.transferobject.VoucherTO;
import br.com.adscaller.util.AlertAndroid;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class CardActivity extends AppCompatActivity {

    private static int REQUEST_CODE_SCAN_CARD = 1;

    Activity activity = this;

    RecyclerView recyclerView;
    View emptyLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setTitle(R.string.my_cards);

        setContentView(R.layout.activity_card);

        emptyLayout = findViewById(R.id.empty_layout);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scanIntent = new Intent(activity, CardIOActivity.class);

                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true);
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true);

                startActivityForResult(scanIntent, REQUEST_CODE_SCAN_CARD);
            }
        });

        updateCardInfo();

    }

    private void updateCardInfo() {

        List<CardTO> cardTOList = new ArrayList<>();
        List<CardPO> cardPOList = new CardDAO(this).findAll();
        for (CardPO cardPO: cardPOList) {
            cardTOList.add(cardPO.getCardTO());
        }


        if (cardTOList.isEmpty()) {

            emptyLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

        } else {

            emptyLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            recyclerView.setAdapter(new CardAdapter(cardTOList, this));

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SCAN_CARD) {

            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {

                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                CardTO cardTO = new CardTO();
                cardTO.setUserId(UserPrefs.getUser(activity).getId());
                cardTO.setCardNumber(scanResult.cardNumber);
                cardTO.setRedactedCardNumber(scanResult.getRedactedCardNumber());
                cardTO.setCardholderName(scanResult.cardholderName);
                cardTO.setCvv(scanResult.cvv);

                if (scanResult.expiryMonth < 10) {
                    cardTO.setExpiryMonth("0" + scanResult.expiryMonth);
                } else {
                    cardTO.setExpiryMonth(String.valueOf(scanResult.expiryMonth));
                }

                CardDAO cardDAO = new CardDAO(activity);
                CardPO existingCard = cardDAO.findByNumber(cardTO.getCardNumber());
                if (existingCard != null) {
                    AlertAndroid.showMessageDialog(activity, R.string.card_already_added);
                } else {
                    cardDAO.create(new CardPO(cardTO));
                }

                List<CardPO> allCards = cardDAO.findAll();

                if (allCards.size() == 1) {
                    UserTO userTO = UserPrefs.getUser(activity);
                    userTO.setSelectedCard(allCards.get(0).getCardTO().getPrimaryKey());
                    UserPrefs.setUser(activity, userTO);
                }

                updateCardInfo();

            }
        }
    }


}
