package br.com.adscaller.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.adscaller.UserPrefs;
import br.com.adscaller.util.StringHelper;

public class SplashActivity extends AppCompatActivity {

    private int SPLASH_TIME_OUT = 2000;

    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (StringHelper.isBlank(UserPrefs.getUser(activity).getLogin())) {
                    Intent i = new Intent(activity, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(activity, MainActivity.class);
                    startActivity(i);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
