package br.com.adscaller.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.adapter.OrderAdapter;
import br.com.adscaller.adapter.OrderItemAdapter;
import br.com.adscaller.transferobject.OrderItemTO;
import br.com.adscaller.transferobject.OrderTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OrderDetailActivity extends AppCompatActivity {

    OrderTO orderTO;

    OrderTask orderTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        orderTO = (OrderTO) getIntent().getSerializableExtra(OrderTO.PARAM);

        TextView viewDate = findViewById(R.id.textview_date);
        TextView viewStatus = findViewById(R.id.textview_status);
        TextView viewCode = findViewById(R.id.textview_code);
        TextView viewTotal = findViewById(R.id.textview_total);

        String data = "";
        try {

            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                    orderTO.getDate().replace("T", " "));

            data = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        viewDate.setText(data);
        viewStatus.setText(orderTO.getStatus());
        viewCode.setText(orderTO.getOrderNumber());

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        viewTotal.setText(format.format(orderTO.getTotalPrice()));

        ImageView imageView = findViewById(R.id.imageview);

        try {

            Bitmap bitmap = textToImage(orderTO.getOrderNumber());
            imageView.setImageBitmap(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (orderTask != null) {
            orderTask.cancel(true);
        }
        orderTask = new OrderTask(this);
        orderTask.execute();

    }

    private void refreshItems(OrderTO orderTO) {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new OrderItemAdapter(orderTO.getOrderItems(), this));
    }

    private Bitmap textToImage(String text) throws WriterException, NullPointerException {

        int width = 250;
        int heigth = 250;

        BitMatrix bitMatrix;

        try {
            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.DATA_MATRIX.QR_CODE,
                    width, heigth, null);
        } catch (IllegalArgumentException Illegalargumentexception) {
            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        int colorWhite = 0xFFFFFFFF;
        int colorBlack = 0xFF000000;

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }


    class OrderTask extends AsyncTask<Void, String, Boolean> {

        Activity activity;

        public OrderTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build();

                Gson gson = new GsonBuilder().create();

                String url = "http://adscallerapi.azurewebsites.net/api/Order/reportdetail/" +
                        UserPrefs.getUser(activity).getToken() + "/" + orderTO.getId();

                LogHelper.log("getOrderDetail", url);

                Request request= new Request.Builder()
                        .url(url)
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {

                    String apiResponse = responseBody.string();

                    if (StringHelper.isNotBlank(apiResponse)) {

                        orderTO = gson.fromJson(apiResponse, OrderTO.class);

                        return true;

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                LogHelper.log(e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (!isCancelled()) {

                if (!success) {

                    AlertAndroid.showMessageDialog(activity, R.string.service_error);

                } else {

                    refreshItems(orderTO);

                }
            }

        }

    }




}
