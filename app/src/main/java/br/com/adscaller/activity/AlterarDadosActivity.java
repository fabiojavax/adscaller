package br.com.adscaller.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import br.com.adscaller.Constants;
import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.task.CadastroTask;
import br.com.adscaller.task.GetUserTask;
import br.com.adscaller.task.PutUserTask;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AWSCognitoUtil;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.IOUtils;
import br.com.adscaller.util.StringHelper;

public class AlterarDadosActivity extends AppCompatActivity {

    Activity activity = this;

    private EditText editTextNome;
    private EditText editTextEmail;
    private EditText editTextTelefone;
    private EditText editTextNascimento;
    private RadioButton radioMasculino;
    private RadioButton radioFeminino;
    private EditText editTextSenha;
    private CheckBox checkBoxAceite;

    //S3 OBJECTS
    private static int PICK_IMAGE_CAMERA = 1;
    private static int PICK_IMAGE_GALLERY = 2;
    String photoPath;
    File sourceFile;
    ProgressDialog progressDialog;
    String nomeEnviado;
    private TransferUtility transferUtility;
    ImageView imageViewProfile;

    TextView textViewButton;

    private BroadcastReceiver userReceiver;
    GetUserTask getUserTask;
    PutUserTask putUserTask;
    CadastroTask cadastroTask;

    boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_dados);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setTitle("Meus Dados");

        isUpdate = getIntent().getBooleanExtra("update", false);

        imageViewProfile = findViewById(R.id.imageview_profile);
        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeProfilePicture();
            }
        });

        editTextNome = findViewById(R.id.edittext_nome);
        editTextEmail = findViewById(R.id.edittext_email);
        editTextTelefone = findViewById(R.id.edittext_telefone);
        editTextNascimento = findViewById(R.id.edittext_nascimento);
        new DateInputMask(editTextNascimento);
        radioMasculino = findViewById(R.id.radio_masculino);
        radioFeminino = findViewById(R.id.radio_feminino);
        editTextSenha = findViewById(R.id.edittext_senha);
        checkBoxAceite = findViewById(R.id.checkbox_aceite);

        textViewButton = findViewById(R.id.textview_button);

        if (getIntent() != null && getIntent().getBooleanExtra("update", false)) {
            textViewButton.setText(getString(R.string.update));
        }

        findViewById(R.id.button_cadastrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((!validateFields() || !validateRadios()) && !isUpdate) {
                    AlertAndroid.showMessageDialog(activity, R.string.fill_all_fields);
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()) {
                    AlertAndroid.showMessageDialog(activity, R.string.valid_email_or_phone);
                } else if (!validateDate()) {
                    AlertAndroid.showMessageDialog(activity, R.string.invalid_date);
                } else {

                    String nome = editTextNome.getText().toString();
                    String email = editTextEmail.getText().toString();
                    String telefone = editTextTelefone.getText().toString();
                    String nascimento = editTextNascimento.getText().toString();
                    String sexo = "1";
                    if (radioFeminino.isChecked()) {
                        sexo = "2";
                    }

                    String senha = editTextSenha.getText().toString();

                    if (isUpdate) {

                        if (putUserTask != null) {
                            putUserTask.cancel(true);
                        }
                        putUserTask = new PutUserTask(activity, nome, senha, email, telefone,
                                nascimento, sexo, Constants.AWS_ROOT + nomeEnviado);
                        putUserTask.execute();


                    } else {

                        if (cadastroTask != null) {
                            cadastroTask.cancel(true);
                        }
                        cadastroTask = new CadastroTask(activity, nome, senha, email, telefone,
                                nascimento, sexo, Constants.AWS_ROOT + nomeEnviado);
                        cadastroTask.execute();

                    }

                }



            }
        });

        transferUtility = AWSCognitoUtil.getTransferUtility(this);

        if (isUpdate) {

            if (getUserTask != null) {
                getUserTask.cancel(true);
            }
            getUserTask = new GetUserTask(activity);
            getUserTask.execute();

            userReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    fillUserData();
                }
            };
            LocalBroadcastManager.getInstance(activity).registerReceiver(userReceiver,
                    new IntentFilter("fill.user.data"));

        }

        Picasso.with(activity).load(UserPrefs.getUser(activity).getAvatar()).into(imageViewProfile);


    }

    public void onDestroy() {
        super.onDestroy();
        if (userReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(userReceiver);
        }
    }

    private void fillUserData() {

        UserTO userTO = UserPrefs.getUser(this);

        editTextNome.setText(userTO.getName());
        editTextEmail.setText(userTO.getEmail());
        editTextTelefone.setText(userTO.getPhone());
        editTextNascimento.setText(userTO.getBirthDate());
        Integer gender = userTO.getGender();
        if (gender == 0) {
            radioMasculino.setChecked(true);
        } else {
            radioFeminino.setChecked(true);
        }

    }

    private boolean validateFields() {

        if (StringHelper.isBlank(editTextNome.getText().toString()) ||
                StringHelper.isBlank(editTextEmail.getText().toString()) ||
                StringHelper.isBlank(editTextTelefone.getText().toString()) ||
                StringHelper.isBlank(editTextNascimento.getText().toString()) ||
                StringHelper.isBlank(editTextSenha.getText().toString())) {
            return false;
        }

        return true;

    }

    private boolean validateRadios() {

        return radioMasculino.isChecked() || radioFeminino.isChecked();

    }

    private boolean validateDate() {
        try {
            new SimpleDateFormat("dd/MM/yyyy").parse(editTextNascimento.getText().toString());
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    private boolean validateAcceptTerms() {
        return checkBoxAceite.isChecked();
    }


    public void changeProfilePicture() {

        final CharSequence[] options = {getString(R.string.camera),
                getString(R.string.gallery), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity,
                                new String[]{Manifest.permission.CAMERA}, PICK_IMAGE_CAMERA);
                    } else {
                        launchCamera();
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PICK_IMAGE_GALLERY);
                    } else {
                        launchGallery();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    public void launchCamera() {

        photoPath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/adscaller_" + new Date().getTime() + ".jpg";
        File file = new File(photoPath);

        Uri photoUri;
        if (Build.VERSION.SDK_INT >= 24) {
            photoUri = FileProvider.getUriForFile(this, Constants.FILE_PROVIDER, file);

        } else {
            photoUri = Uri.fromFile(file);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        intent.putExtra("outputFormat", "jpg");
        startActivityForResult(intent, PICK_IMAGE_CAMERA);

    }

    public void launchGallery() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_CAMERA) {

            try {

                Uri photoUri = Uri.fromFile(new File(photoPath));

                CropImageView.CropShape shape = CropImageView.CropShape.OVAL;

                CropImage.activity(photoUri)
                        .setCropShape(shape)
                        .setInitialCropWindowPaddingRatio(0)
                        .setAllowFlipping(false)
                        .setFixAspectRatio(true)
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .setActivityTitle(getString(R.string.cortar_foto))
                        .start(activity);

            } catch (Exception e) {

                AlertAndroid.showMessageDialog(activity, R.string.camera_error);

            }

        }

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_GALLERY) {

            Uri photoUri = data.getData();

            CropImageView.CropShape shape = CropImageView.CropShape.OVAL;

            CropImage.activity(photoUri)
                    .setCropShape(shape)
                    .setInitialCropWindowPaddingRatio(0)
                    .setAllowFlipping(false)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.OFF)
                    .setActivityTitle(getString(R.string.cortar_foto))
                    .start(activity);
        }

        if (resultCode == Activity.RESULT_OK && requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri imageUri = result.getUri();
            sourceFile = new File(imageUri.getPath());
            uploadFile(sourceFile);
        }

    }

    private void uploadFile(File file) {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.wait));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        IOUtils.compressPhoto(sourceFile, 750, 750);

        nomeEnviado = UUID.randomUUID().toString() + new Date().getTime();

        String extensao = "";
        if (file.getName().contains(".")) {
            extensao = file.getName().substring(file.getName().indexOf("."));
        }
        nomeEnviado += extensao;

        TransferObserver observer = transferUtility.upload("youback", nomeEnviado, file);

        observer.setTransferListener(new UploadListener());

    }

    private class UploadListener implements TransferListener {

        @Override
        public void onError(int id, Exception e) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(activity);
            }
            builder.setTitle(R.string.app_name)
                    .setMessage(activity.getString(R.string.send_fail))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            if (newState.equals(TransferState.COMPLETED)) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Picasso.with(activity).load(Constants.AWS_ROOT + nomeEnviado).into(imageViewProfile);

            }

        }


    }


}
