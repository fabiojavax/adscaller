package br.com.adscaller.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.com.adscaller.fragment.ContactsFragment;

public class ContactsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.content, ContactsFragment.newInstance("true"),
                        ContactsFragment.FRAGMENT_TAG).disallowAddToBackStack().commit();
    }

}
