package br.com.adscaller.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import br.com.adscaller.R;
import br.com.adscaller.storage.BannerDAO;
import br.com.adscaller.storage.BannerPO;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.transferobject.BannerTO;
import br.com.adscaller.transferobject.KartTO;

public class BannerActivity extends AppCompatActivity {

    Activity activity = this;

    ImageView imageView;
    TextView textViewCountdown;

    BannerTO bannerTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);

        imageView = findViewById(R.id.imageview);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.layout_quero).setVisibility(View.GONE);
                findViewById(R.id.layout_adicionado).setVisibility(View.VISIBLE);

                findViewById(R.id.imageview).setVisibility(View.GONE);
                findViewById(R.id.gifview).setVisibility(View.VISIBLE);

                try {

                    KartDAO kartDAO = new KartDAO(activity);

                    Integer promotionId = bannerTO.getPromotion().getPromotionId();

                    KartPO existingKart = kartDAO.findByPromotion(promotionId);

                    if (existingKart == null) {

                        KartTO kartTO = new KartTO();
                        kartTO.setPromotionId(promotionId);
                        kartTO.setQuantity(1);
                        kartTO.setBannerUrl(bannerTO.getBannerUrl());
                        kartTO.setSellerName(bannerTO.getPromotion().getSeller().getName());
                        kartTO.setDescription(bannerTO.getPromotion().getDescription());
                        kartTO.setPrice(bannerTO.getPromotion().getPrice());
                        if (bannerTO.getPromotion().getPercentualDiscount() == null ||
                                bannerTO.getPromotion().getPercentualDiscount() == 0) {
                            kartTO.setPriceDiscount(kartTO.getPrice());
                        } else {
                            Integer percentualDiscount = bannerTO.getPromotion().getPercentualDiscount();
                            Double priceDiscount = kartTO.getPrice() - ((kartTO.getPrice() / 100) * percentualDiscount);
                            kartTO.setPriceDiscount(priceDiscount);
                        }

                        kartTO.setPercentualDiscount(bannerTO.getPromotion().getPercentualDiscount());
                        kartDAO.create(new KartPO(kartTO));

                    } else {

                        existingKart.getKartTO().setQuantity(existingKart.getKartTO().getQuantity() + 1);
                        kartDAO.update(existingKart);

                    }

                } catch (Exception e) {e.printStackTrace();}

            }
        });

        textViewCountdown = findViewById(R.id.textview_countdown);

        BannerDAO bannerDAO = new BannerDAO(activity);
        List<BannerPO> bannerPOList = bannerDAO.findAll();

        for (BannerPO bannerPO: bannerPOList) {
            if (bannerPO.getBannerTO().getBannerUrl().toLowerCase().contains("microsoft")) {
                int pos = bannerPO.getBannerTO().getBannerUrl().indexOf("https");
                bannerPO.getBannerTO().setBannerUrl(bannerPO.getBannerTO().getBannerUrl().substring(pos));
            }
        }

        int randomPick = new Random().nextInt(bannerPOList.size());

        bannerTO = bannerPOList.get(randomPick).getBannerTO();

        Picasso.with(activity).load(bannerTO.getBannerUrl()).into(imageView, new Callback() {
            @Override
            public void onSuccess() {

                findViewById(R.id.progress).setVisibility(View.GONE);

                new CountDownTimer(4000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        textViewCountdown.setText(String.valueOf(millisUntilFinished / 1000));
                    }



                    public void onFinish() {
                        Intent data = new Intent();
                        setResult(RESULT_OK, data);
                        finish();
                    }
                }.start();
            }

            @Override
            public void onError() {
                Intent data = new Intent();
                setResult(RESULT_OK, data);
                finish();
            }
        });






    }
}
