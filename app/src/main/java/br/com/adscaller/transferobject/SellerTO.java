package br.com.adscaller.transferobject;

import java.io.Serializable;
import java.util.List;

public class SellerTO implements Serializable{

    private Integer sellerId;
    private String name;
    private List<Integer> categories;

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }
}
