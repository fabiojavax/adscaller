package br.com.adscaller.transferobject;

import java.io.Serializable;

public class BannerTO implements Serializable {

    private Integer primaryKey;

    private Integer bannerId;
    private String bannerUrl;
    private boolean enabled;
    private Integer promotionId;
    private PromotionTO promotion;

    public Integer getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Integer primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Integer getBannerId() {
        return bannerId;
    }

    public void setBannerId(Integer bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }


    public PromotionTO getPromotion() {
        return promotion;
    }

    public void setPromotion(PromotionTO promotion) {
        this.promotion = promotion;
    }


}
