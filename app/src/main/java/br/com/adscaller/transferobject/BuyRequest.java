package br.com.adscaller.transferobject;

import java.util.List;

public class BuyRequest {

    private Integer userId;
    private String voucher;
    private Double price;
    private CreditCardTO creditCard;
    private List<BuyItemTO> orderItems;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public CreditCardTO getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCardTO creditCard) {
        this.creditCard = creditCard;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<BuyItemTO> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<BuyItemTO> orderItems) {
        this.orderItems = orderItems;
    }
}
