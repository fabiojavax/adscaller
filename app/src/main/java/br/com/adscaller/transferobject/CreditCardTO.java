package br.com.adscaller.transferobject;

public class CreditCardTO {

    private Integer creditCardBrand = 0;
    private String creditCardNumber;
    private String expireMonth;
    private String expireYear;
    private String holderName;
    private Integer securityCode;

    public Integer getCreditCardBrand() {
        return creditCardBrand;
    }

    public void setCreditCardBrand(Integer creditCardBrand) {
        this.creditCardBrand = creditCardBrand;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(String expireMonth) {
        this.expireMonth = expireMonth;
    }

    public String getExpireYear() {
        return expireYear;
    }

    public void setExpireYear(String expireYear) {
        this.expireYear = expireYear;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public Integer getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(Integer securityCode) {
        this.securityCode = securityCode;
    }

}
