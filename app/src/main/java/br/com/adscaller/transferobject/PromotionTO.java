package br.com.adscaller.transferobject;

import java.io.Serializable;

public class PromotionTO implements Serializable{

    private Integer promotionId;
    private String title;
    private String description;
    private Double price;
    private Integer percentualDiscount;
    private String validUntil;
    private String validSince;
    private String dtCreated;
    private Integer status;
    private Integer sellerId;
    private Integer qtd;
    private String obs;
    private SellerTO seller;
    private String[] banners;

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getPercentualDiscount() {
        return percentualDiscount;
    }

    public void setPercentualDiscount(Integer percentualDiscount) {
        this.percentualDiscount = percentualDiscount;
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }

    public String getValidSince() {
        return validSince;
    }

    public void setValidSince(String validSince) {
        this.validSince = validSince;
    }

    public String getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(String dtCreated) {
        this.dtCreated = dtCreated;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getQtd() {
        return qtd;
    }

    public void setQtd(Integer qtd) {
        this.qtd = qtd;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public SellerTO getSeller() {
        return seller;
    }

    public void setSeller(SellerTO seller) {
        this.seller = seller;
    }

    public String[] getBanners() {
        return banners;
    }

    public void setBanners(String[] banners) {
        this.banners = banners;
    }
}
