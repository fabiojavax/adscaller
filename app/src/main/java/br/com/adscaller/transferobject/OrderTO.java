package br.com.adscaller.transferobject;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderTO implements Serializable {

    public static final String PARAM = "OrderTOParam";
    private Integer id;
    private String orderNumber;
    private String status;
    private Double totalPrice;
    @SerializedName("dtCreated")
    private String date;

    private List<OrderItemTO> orderDetailPromotions;


    private String itemsDescription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<OrderItemTO> getOrderItems() {
        return orderDetailPromotions;
    }

    public void setOrderItems(List<OrderItemTO> orderItems) {
        this.orderDetailPromotions = orderItems;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getItemsDescription() {
        return itemsDescription;
    }

    public void setItemsDescription(String itemsDescription) {
        this.itemsDescription = itemsDescription;
    }
}
