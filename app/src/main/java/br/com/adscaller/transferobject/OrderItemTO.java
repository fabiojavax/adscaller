package br.com.adscaller.transferobject;

import java.io.Serializable;

public class OrderItemTO implements Serializable {

    private Integer id;
    private String title;
    private Integer qtd = 0;
    private Double price;
    private Double discount;
    private String sellerName;
    private String sellerCnpj;
    private String sellerAddress;
    private boolean hasVoucher;
    private Double discountVoucher;
    private String orderItemCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getQtd() {
        return qtd;
    }

    public void setQtd(Integer qtd) {
        this.qtd = qtd;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerCnpj() {
        return sellerCnpj;
    }

    public void setSellerCnpj(String sellerCnpj) {
        this.sellerCnpj = sellerCnpj;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public boolean isHasVoucher() {
        return hasVoucher;
    }

    public void setHasVoucher(boolean hasVoucher) {
        this.hasVoucher = hasVoucher;
    }

    public Double getDiscountVoucher() {
        return discountVoucher;
    }

    public void setDiscountVoucher(Double discountVoucher) {
        this.discountVoucher = discountVoucher;
    }

    public String getOrderItemCode() {
        return orderItemCode;
    }

    public void setOrderItemCode(String orderItemCode) {
        this.orderItemCode = orderItemCode;
    }
}
