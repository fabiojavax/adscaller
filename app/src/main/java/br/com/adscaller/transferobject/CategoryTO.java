package br.com.adscaller.transferobject;

public class CategoryTO {

    private Integer id;
    private String name;
    private boolean disabled;
    private String imageUrlOn;
    private String imageUrlOff;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getImageUrlOn() {
        return imageUrlOn;
    }

    public void setImageUrlOn(String imageUrlOn) {
        this.imageUrlOn = imageUrlOn;
    }

    public String getImageUrlOff() {
        return imageUrlOff;
    }

    public void setImageUrlOff(String imageUrlOff) {
        this.imageUrlOff = imageUrlOff;
    }
}
