package br.com.adscaller.transferobject;

import java.io.Serializable;
import java.util.Objects;

public class ContactTO implements Serializable {

    public static String PARAM = "ContactTOParam";

    private Integer primaryKey;
    private String name;
    private String phone;

    public ContactTO() {

    }

    public ContactTO(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public Integer getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Integer primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactTO contactTO = (ContactTO) o;
        return Objects.equals(name, contactTO.name) &&
                Objects.equals(phone, contactTO.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phone);
    }

}
