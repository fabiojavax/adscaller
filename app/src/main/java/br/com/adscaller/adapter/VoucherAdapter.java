package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.activity.MainActivity;
import br.com.adscaller.storage.RecentsDAO;
import br.com.adscaller.storage.RecentsPO;
import br.com.adscaller.transferobject.ContactTO;
import br.com.adscaller.transferobject.VoucherTO;
import br.com.adscaller.util.StringHelper;

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.ContactsHolder> {

    private List<VoucherTO> list;
    private List<VoucherTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public VoucherAdapter(List<VoucherTO> list, Activity activity) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_vouchers, parent, false);
        return new ContactsHolder(mainView);
    }

    public class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewDescription;
        public TextView viewCode;
        public TextView viewExpiration;

        public ContactsHolder(View itemView) {

            super(itemView);

            viewDescription = itemView.findViewById(R.id.textview_description);
            viewCode = itemView.findViewById(R.id.textview_code);
            viewExpiration = itemView.findViewById(R.id.textview_expiration);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final ContactsHolder holder, int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final VoucherTO voucherTO = list.get(position);

            holder.viewDescription.setText(voucherTO.getPromotion());
            holder.viewCode.setText(voucherTO.getCode());
            holder.viewExpiration.setText(voucherTO.getExpiration());

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
