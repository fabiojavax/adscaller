package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.activity.PromotionActivity;
import br.com.adscaller.storage.CardDAO;
import br.com.adscaller.storage.CardPO;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.transferobject.KartTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.StringHelper;

public class KartAdapter extends RecyclerView.Adapter<KartAdapter.KartHolder> {

    private List<KartTO> list;
    private List<KartTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public KartAdapter(Activity activity, List<KartTO> list) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public KartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_campaign, parent, false);
        return new KartHolder(mainView);
    }

    public class KartHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView viewImage;
        public TextView viewSellerName;
        public TextView viewDescription;
        public TextView viewPrice;
        public TextView viewPriceDiscount;
        public TextView viewDiscount;

        public KartHolder(View itemView) {

            super(itemView);

            viewImage = itemView.findViewById(R.id.imageview);
            viewSellerName = itemView.findViewById(R.id.textview_seller_name);
            viewDescription = itemView.findViewById(R.id.textview_description);
            viewPrice = itemView.findViewById(R.id.textview_price);
            viewPriceDiscount = itemView.findViewById(R.id.textview_price_discount);
            viewDiscount = itemView.findViewById(R.id.textview_discount);

            viewPrice.setPaintFlags(viewPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

        }

    }

    @Override
    public void onBindViewHolder(final KartHolder holder, final int position) {

        if (list.get(position) != null) {

            final KartTO kartTO = list.get(position);

            holder.viewSellerName.setText(kartTO.getSellerName());

            holder.viewDescription.setText(kartTO.getDescription());

            if (StringHelper.isNotBlank(kartTO.getBannerUrl())) {

                if (kartTO.getBannerUrl().toLowerCase().contains("microsoft")) {
                    int pos = kartTO.getBannerUrl().indexOf("https");
                    kartTO.setBannerUrl( kartTO.getBannerUrl().substring(pos));
                }

                Picasso.with(activity).load(kartTO.getBannerUrl()).fit().into(holder.viewImage);

            } else {

                holder.viewImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));

            }

            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());


            Double price = kartTO.getPrice() != null ? kartTO.getPrice() : 0;
            Double priceDiscount = kartTO.getPriceDiscount() != null ? kartTO.getPriceDiscount() : 0;

            holder.viewPrice.setText(format.format(price));
            holder.viewPriceDiscount.setText(format.format(priceDiscount));

            if (kartTO.getPercentualDiscount() == null || kartTO.getPercentualDiscount() == 0) {

                Integer percentualDiscount = (int) (100 - (priceDiscount * 100) / price);
                holder.viewDiscount.setText("-" + percentualDiscount + "%");

            } else {
                holder.viewDiscount.setText("-" + kartTO.getPercentualDiscount() + "%");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(activity, PromotionActivity.class);
//                    intent.putExtra("kartTO", kartTO);
//                    activity.startActivity(intent);
                }
            });

            holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(activity.getString(R.string.remove_kart_item)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            new KartDAO(activity).removeByPK(new KartPO(kartTO));
                            list.remove(position);
                            notifyDataSetChanged();
                            return true;
                        }
                    });
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
