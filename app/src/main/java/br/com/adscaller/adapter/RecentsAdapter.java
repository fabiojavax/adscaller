package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.activity.MainActivity;
import br.com.adscaller.storage.FavoriteDAO;
import br.com.adscaller.storage.FavoritePO;
import br.com.adscaller.storage.RecentsDAO;
import br.com.adscaller.storage.RecentsPO;
import br.com.adscaller.transferobject.ContactTO;
import br.com.adscaller.util.StringHelper;

public class RecentsAdapter extends RecyclerView.Adapter<RecentsAdapter.FavoriteHolder> {

    private List<ContactTO> list;
    private List<ContactTO> copyList = new ArrayList<>();

    private Context mContext;
    private Activity activity;

    View mainView;

    public RecentsAdapter(List<ContactTO> list, Activity activity) {
        this.list = list;
        this.copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public FavoriteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mainView = LayoutInflater.from(mContext).inflate(R.layout.adapter_recents, parent, false);
        return new FavoriteHolder(mainView);
    }

    public class FavoriteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewAvatarName;
        public TextView viewName;
        public TextView viewPhone;
        public ImageView viewImage;

        public FavoriteHolder(View itemView) {
            super(itemView);

            viewAvatarName = itemView.findViewById(R.id.textview_avatar_name);
            viewName = itemView.findViewById(R.id.textview_name);
            viewPhone = itemView.findViewById(R.id.textview_phone);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final FavoriteHolder holder, final int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final ContactTO contactTO = list.get(position);

            holder.viewName.setText(contactTO.getName());
            holder.viewPhone.setText(contactTO.getPhone());

            if (StringHelper.isNotBlank(contactTO.getName()) && contactTO.getName().length() > 2) {

                if (contactTO.getName().contains(" ")) {

                    String[] tokens = contactTO.getName().split(" ");

                    holder.viewAvatarName.setText(tokens[0].substring(0, 1).toUpperCase()
                            + tokens[1].substring(0, 1).toUpperCase());

                } else {

                    holder.viewAvatarName.setText(contactTO.getName().substring(0, 2).toUpperCase());

                }

            } else {
                holder.viewAvatarName.setText("UN");
            }

            holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(activity.getString(R.string.remove_from_recents)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            new RecentsDAO(activity).removeByPK(new RecentsPO(contactTO));
                            list.remove(position);
                            Intent refreshFavoritesBroadcast = new Intent("refresh.fragment.recents");
                            LocalBroadcastManager.getInstance(activity).sendBroadcast(refreshFavoritesBroadcast);
                            return true;
                        }
                    });
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContactTO newContactTO = new ContactTO();
                    newContactTO.setName(contactTO.getName());
                    newContactTO.setPhone(contactTO.getPhone());
                    new RecentsDAO(activity).create(new RecentsPO(newContactTO));

                    ((MainActivity)activity).setCurrentContact(contactTO, 1);

                    Intent refreshFavoritesBroadcast = new Intent("refresh.fragment.recents");
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(refreshFavoritesBroadcast);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void filter(String text) {
        list.clear();
        if(text.isEmpty()){
            list.addAll(copyList);
        } else{
            text = text.toLowerCase();
            for(ContactTO item: copyList){
                if(item.getName().toLowerCase().contains(text) || item.getPhone().contains(text)){
                    list.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

}
