package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.activity.OrderDetailActivity;
import br.com.adscaller.transferobject.OrderTO;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ContactsHolder> {

    private List<OrderTO> list;
    private List<OrderTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public OrderAdapter(List<OrderTO> list, Activity activity) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_order, parent, false);
        return new ContactsHolder(mainView);
    }

    public class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewDescription;
        public TextView viewDay;
        public TextView viewMonth;
        public TextView viewYear;
        public TextView viewTotal;

        public ContactsHolder(View itemView) {

            super(itemView);

            viewDescription = itemView.findViewById(R.id.textview_description);
            viewDay = itemView.findViewById(R.id.textview_day);
            viewYear = itemView.findViewById(R.id.textview_year);
            viewMonth = itemView.findViewById(R.id.textview_month);
            viewTotal = itemView.findViewById(R.id.textview_total);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final ContactsHolder holder, int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final OrderTO orderTO = list.get(position);

            try {

                Date date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(
                        orderTO.getDate().split("T")[0]);
                String day = new SimpleDateFormat("dd", Locale.getDefault()).format(date);
                String month = new SimpleDateFormat("MMM", Locale.getDefault()).format(date);
                String year = new SimpleDateFormat("yyyy", Locale.getDefault()).format(date);

                holder.viewDay.setText(day);
                holder.viewMonth.setText(month);
                holder.viewYear.setText(year);

                holder.viewDescription.setText(orderTO.getItemsDescription());

                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
                holder.viewTotal.setText("Total: " + format.format(orderTO.getTotalPrice()));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OrderDetailActivity.class);
                        intent.putExtra(OrderTO.PARAM, orderTO);
                        activity.startActivity(intent);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
