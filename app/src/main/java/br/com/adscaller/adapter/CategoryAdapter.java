package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.transferobject.CategoryTO;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.StringHelper;
import de.hdodenhof.circleimageview.CircleImageView;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ContactsHolder> {

    private List<CategoryTO> list;
    private List<CategoryTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public CategoryAdapter(List<CategoryTO> list, Activity activity) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_category, parent, false);
        return new ContactsHolder(mainView);
    }

    public class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewCategory;
        public CircleImageView viewImage;

        public ContactsHolder(View itemView) {

            super(itemView);

            viewCategory = itemView.findViewById(R.id.textview_category);
            viewImage = itemView.findViewById(R.id.imageview_category);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final ContactsHolder holder, final int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final CategoryTO categoryTO = list.get(position);

            final UserTO userTO = UserPrefs.getUser(context);

            holder.viewCategory.setText(categoryTO.getName());

            if (StringHelper.isNotBlank(categoryTO.getImageUrlOn())) {

                if (categoryTO.getId().equals(userTO.getSelectedCategory())) {

                    Picasso.with(activity).load(categoryTO.getImageUrlOn()).fit().into(holder.viewImage);

                } else {

                    Picasso.with(activity).load(categoryTO.getImageUrlOff()).fit().into(holder.viewImage);

                }

            } else{

                try {

                    String nome = StringHelper.retirarAcentos(categoryTO.getName()).toLowerCase();

                    if (categoryTO.getId().equals(userTO.getSelectedCategory())) {
                        nome += "_active";
                    }

                    int resourceId = context.getResources().getIdentifier(nome,
                            "drawable", context.getPackageName());
                    holder.viewImage.setImageDrawable(context.getResources().getDrawable(resourceId));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userTO.setSelectedCategory(categoryTO.getId());
                    UserPrefs.setUser(context, userTO);
                    notifyDataSetChanged();
                    Intent refreshFavoritesBroadcast = new Intent("refresh.recycler.promotion");
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(refreshFavoritesBroadcast);

                }
            });



        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
