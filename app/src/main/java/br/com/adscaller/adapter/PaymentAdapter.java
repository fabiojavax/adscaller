package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.activity.PaymentActivity;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.transferobject.KartTO;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ContactsHolder> {

    private List<KartTO> list;
    private List<KartTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public PaymentAdapter(List<KartTO> list, Activity activity) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_payment, parent, false);
        return new ContactsHolder(mainView);
    }

    public class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewDescription;
        public TextView viewQuantity;
        public TextView viewValue;
        public TextView viewTotal;

        public ContactsHolder(View itemView) {

            super(itemView);

            viewDescription = itemView.findViewById(R.id.textview_description);
            viewQuantity = itemView.findViewById(R.id.textview_quantity);
            viewValue = itemView.findViewById(R.id.textview_value);
            viewTotal = itemView.findViewById(R.id.textview_total);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final ContactsHolder holder, final int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final KartTO kartTO = list.get(position);

            holder.viewDescription.setText(kartTO.getDescription());
            holder.viewQuantity.setText(kartTO.getQuantity() + "x");

            Double price = kartTO.getPriceDiscount();
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            holder.viewValue.setText(format.format(price));
            holder.viewTotal.setText(format.format(price * kartTO.getQuantity()));

            /*
            holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(activity.getString(R.string.remove_kart_item)).setOnMenuItemClickListener(
                            new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            new KartDAO(activity).removeByPK(new KartPO(kartTO));
                            list.remove(position);
                            notifyDataSetChanged();
                            ((PaymentActivity)activity).updateTotal();
                            return true;
                        }
                    });
                }
            });*/

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
