package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.activity.PromotionActivity;
import br.com.adscaller.transferobject.BannerTO;
import br.com.adscaller.util.StringHelper;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.CampaignHolder> {

    private List<BannerTO> list;
    private List<BannerTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public PromotionAdapter(Activity activity, List<BannerTO> list) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public CampaignHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_campaign, parent, false);
        return new CampaignHolder(mainView);
    }

    public class CampaignHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView viewImage;
        public TextView viewSellerName;
        public TextView viewDescription;
        public TextView viewPrice;
        public TextView viewPriceDiscount;
        public TextView viewDiscount;

        public CampaignHolder(View itemView) {

            super(itemView);

            viewImage = itemView.findViewById(R.id.imageview);
            viewSellerName = itemView.findViewById(R.id.textview_seller_name);
            viewDescription = itemView.findViewById(R.id.textview_description);
            viewPrice = itemView.findViewById(R.id.textview_price);
            viewPriceDiscount = itemView.findViewById(R.id.textview_price_discount);
            viewDiscount = itemView.findViewById(R.id.textview_discount);

            viewPrice.setPaintFlags(viewPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

        }

    }

    @Override
    public void onBindViewHolder(final CampaignHolder holder, int position) {

        if (list.get(position) != null) {

            final BannerTO bannerTO = list.get(position);

            holder.viewSellerName.setText(bannerTO.getPromotion().getSeller().getName());

            if (bannerTO.getPromotion() != null) {

                holder.viewDescription.setText(bannerTO.getPromotion().getDescription());

            }

            if (StringHelper.isNotBlank(bannerTO.getBannerUrl())) {

                if (bannerTO.getBannerUrl().toLowerCase().contains("microsoft")) {
                    int pos = bannerTO.getBannerUrl().indexOf("https");
                    bannerTO.setBannerUrl( bannerTO.getBannerUrl().substring(pos));
                }

                Picasso.with(activity).load(bannerTO.getBannerUrl()).fit().into(holder.viewImage);

            } else {

                holder.viewImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));

            }

            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());

            Double price = bannerTO.getPromotion().getPrice() != null ?
                    bannerTO.getPromotion().getPrice() : 0;
            holder.viewPrice.setText(format.format(price));

            if (bannerTO.getPromotion().getPercentualDiscount() == null ||
                    bannerTO.getPromotion().getPercentualDiscount() == 0) {

                holder.viewPriceDiscount.setText(format.format(price));

                holder.viewDiscount.setText("-0%");

            } else {


                Integer percentualDiscount = bannerTO.getPromotion().getPercentualDiscount();
                Double priceDiscount = price - ( (price / 100) * percentualDiscount);
                holder.viewPriceDiscount.setText(format.format(priceDiscount));

                holder.viewDiscount.setText("-" + bannerTO.getPromotion().getPercentualDiscount() + "%");

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, PromotionActivity.class);
                    intent.putExtra("bannerTO", bannerTO);
                    activity.startActivity(intent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void filter(String text) {
        list.clear();
        if(text.isEmpty()){
            list.addAll(copyList);
        } else{
            text = text.toLowerCase();
            for(BannerTO item: copyList){
                if(item.getPromotion() != null && item.getPromotion().getDescription().toLowerCase().contains(text)){
                    list.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }



}
