package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.storage.CardDAO;
import br.com.adscaller.storage.CardPO;
import br.com.adscaller.storage.FavoriteDAO;
import br.com.adscaller.storage.FavoritePO;
import br.com.adscaller.transferobject.CardTO;
import br.com.adscaller.transferobject.UserTO;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardHolder> {

    private static String VISA_PATTERN = "4";
    private static String MASTERCARD_PATTERN = "5";

    private List<CardTO> list;
    private List<CardTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public CardAdapter(List<CardTO> list, Activity activity) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_cards, parent, false);
        return new CardHolder(mainView);
    }

    public class CardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewCardNumber;
        public ImageView imageView;
        public ImageView imageViewCheck;

        public CardHolder(View itemView) {

            super(itemView);

            viewCardNumber = itemView.findViewById(R.id.textview_card_number);
            imageView = itemView.findViewById(R.id.imageview);
            imageViewCheck = itemView.findViewById(R.id.imageview_check);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final CardHolder holder, final int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final CardTO cardTO = list.get(position);

            holder.viewCardNumber.setText(cardTO.getRedactedCardNumber());

            if (cardTO.getCardNumber().startsWith(VISA_PATTERN)) {
                Picasso.with(activity).load(R.drawable.visa).fit().into(holder.imageView);
            } else if (cardTO.getCardNumber().startsWith(MASTERCARD_PATTERN)) {
                Picasso.with(activity).load(R.drawable.mastercard).fit().into(holder.imageView);
            } else {
                Picasso.with(activity).load(R.drawable.ic_credit_card_black).fit().into(holder.imageView);
            }

            Integer selectedCard = UserPrefs.getUser(activity).getSelectedCard();

            if (cardTO.getPrimaryKey().equals(selectedCard)) {
                holder.imageViewCheck.setVisibility(View.VISIBLE);
            } else {
                holder.imageViewCheck.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserTO userTO = UserPrefs.getUser(activity);
                    userTO.setSelectedCard(cardTO.getPrimaryKey());
                    UserPrefs.setUser(activity, userTO);
                    notifyDataSetChanged();
                    Snackbar.make(holder.itemView, R.string.main_card_selected,
                            Snackbar.LENGTH_SHORT).show();
                }
            });

            holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(activity.getString(R.string.remove_card)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            new CardDAO(activity).removeByPK(new CardPO(cardTO));
                            list.remove(position);
                            if (list.size() > 0) {
                                UserTO userTO = UserPrefs.getUser(activity);
                                userTO.setSelectedCard(list.get(0).getPrimaryKey());
                                UserPrefs.setUser(activity, userTO);
                            }
                            notifyDataSetChanged();
                            return true;
                        }
                    });
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
