package br.com.adscaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.adscaller.R;
import br.com.adscaller.transferobject.KartTO;
import br.com.adscaller.transferobject.OrderItemTO;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.ContactsHolder> {

    private List<OrderItemTO> list;
    private List<OrderItemTO> copyList = new ArrayList<>();

    private Context context;
    private Activity activity;

    View mainView;

    public OrderItemAdapter(List<OrderItemTO> list, Activity activity) {
        this.list = list;
        copyList.addAll(this.list);
        this.activity = activity;
    }

    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mainView = LayoutInflater.from(context).inflate(R.layout.adapter_payment, parent, false);
        return new ContactsHolder(mainView);
    }

    public class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView viewDescription;
        public TextView viewQuantity;
        public TextView viewValue;
        public TextView viewTotal;
        public TextView viewItemCode;

        public ContactsHolder(View itemView) {

            super(itemView);

            viewDescription = itemView.findViewById(R.id.textview_description);
            viewQuantity = itemView.findViewById(R.id.textview_quantity);
            viewValue = itemView.findViewById(R.id.textview_value);
            viewTotal = itemView.findViewById(R.id.textview_total);
            viewItemCode = itemView.findViewById(R.id.textview_item_code);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {



        }

    }

    @Override
    public void onBindViewHolder(final ContactsHolder holder, final int position) {

        if (list.get(position) == null) {

            return;

        } else {

            final OrderItemTO orderItemTO = list.get(position);

            Integer quantity = 1;
            if (orderItemTO.getQtd() != null && orderItemTO.getQtd() > 0) {
                quantity = orderItemTO.getQtd();
            }

            holder.viewDescription.setText(orderItemTO.getTitle());
            holder.viewItemCode.setText(orderItemTO.getOrderItemCode());

            holder.viewQuantity.setText(quantity + "x");

            Double price = orderItemTO.getPrice();

            if (orderItemTO.getDiscount() != null && orderItemTO.getDiscount() > 0) {

                Double percentualDiscount = orderItemTO.getDiscount();

                price = price - ((price / 100) * percentualDiscount);

            }

            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            holder.viewValue.setText(format.format(price));
            holder.viewTotal.setText(format.format(price * quantity));

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
