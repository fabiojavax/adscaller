package br.com.adscaller.storage;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.adscaller.transferobject.ContactTO;

@SuppressWarnings("serial")
public class RecentsPO extends TransferObject {

    private ContactTO contactTO;

    public interface Mapeamento {

        String TABLE = "recents";
        String PRIMARY_KEY = "_pk";
        String NAME = "name";
        String PHONE = "phone";

        String CREATE = new StringBuilder("create table ").append(TABLE).append(" (")
                .append(PRIMARY_KEY).append(TYPE_PRIMARY_KEY)
                .append(", ").append(NAME).append(TYPE_TEXT)
                .append(", ").append(PHONE).append(TYPE_TEXT)
                .append(");").toString();

    }

    @Override
    public String getTableName() {
        return Mapeamento.TABLE;
    }

    public RecentsPO() {
        super();
    }

    public RecentsPO(ContactTO contactTO) {
        super();
        this.contactTO = contactTO;
    }

    @Override
    public ContentValues getMapping() {

        ContentValues values = new ContentValues();

        values.put(Mapeamento.PRIMARY_KEY, contactTO.getPrimaryKey());
        values.put(Mapeamento.NAME, contactTO.getName());
        values.put(Mapeamento.PHONE, contactTO.getPhone());

        return values;

    }

    @Override
    public void fill(Cursor cursor) {

        contactTO = new ContactTO();

        contactTO.setPrimaryKey(cursor.getInt(cursor.getColumnIndex(Mapeamento.PRIMARY_KEY)));
        contactTO.setName(cursor.getString(cursor.getColumnIndex(Mapeamento.NAME)));
        contactTO.setPhone(cursor.getString(cursor.getColumnIndex(Mapeamento.PHONE)));

    }

    @Override
    public String getId() {
        return contactTO.getPrimaryKey().toString();
    }

    @Override
    public String getColumnId() {
        return Mapeamento.PRIMARY_KEY;
    }

    @Override
    public String getColumnOrder() {
        return Mapeamento.PRIMARY_KEY;
    }

    public ContactTO getContactTO() {
        return contactTO;
    }
}
