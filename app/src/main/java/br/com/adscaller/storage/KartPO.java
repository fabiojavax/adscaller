package br.com.adscaller.storage;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.adscaller.transferobject.CardTO;
import br.com.adscaller.transferobject.KartTO;

@SuppressWarnings("serial")
public class KartPO extends TransferObject {

    private KartTO kartTO;

    public interface Mapeamento {

        String TABLE = "kart";
        String PRIMARY_KEY = "_pk";
        String PROMOTION_ID = "promotion_id";
        String SELLER_NAME = "seller_name";
        String DESCRIPTION = "description";
        String BANNER_URL = "banner_url";
        String PRICE = "price";
        String PRICE_DISCOUNT = "price_discount";
        String PERCENTUAL_DISCOUNT = "percentual_discount";
        String QUANTITY = "quantity";
        String VOUCHER_CODE = "voucher_code";

        String CREATE = new StringBuilder("create table ").append(TABLE).append(" (")
                .append(PRIMARY_KEY).append(TYPE_PRIMARY_KEY)
                .append(", ").append(PROMOTION_ID).append(TYPE_INTEGER)
                .append(", ").append(SELLER_NAME).append(TYPE_TEXT)
                .append(", ").append(DESCRIPTION).append(TYPE_TEXT)
                .append(", ").append(BANNER_URL).append(TYPE_TEXT)
                .append(", ").append(PRICE).append(TYPE_NUMERIC)
                .append(", ").append(PRICE_DISCOUNT).append(TYPE_NUMERIC)
                .append(", ").append(PERCENTUAL_DISCOUNT).append(TYPE_INTEGER)
                .append(", ").append(QUANTITY).append(TYPE_INTEGER)
                .append(", ").append(VOUCHER_CODE).append(TYPE_TEXT)
                .append(");").toString();

    }

    @Override
    public String getTableName() {
        return Mapeamento.TABLE;
    }

    public KartPO() {
        super();
    }

    public KartPO(KartTO kartTO) {
        super();
        this.kartTO = kartTO;
    }

    @Override
    public ContentValues getMapping() {

        ContentValues values = new ContentValues();

        values.put(Mapeamento.PRIMARY_KEY, kartTO.getPrimaryKey());
        values.put(Mapeamento.PROMOTION_ID, kartTO.getPromotionId());
        values.put(Mapeamento.SELLER_NAME, kartTO.getSellerName());
        values.put(Mapeamento.DESCRIPTION, kartTO.getDescription());
        values.put(Mapeamento.BANNER_URL, kartTO.getBannerUrl());
        values.put(Mapeamento.PRICE, kartTO.getPrice());
        values.put(Mapeamento.PRICE_DISCOUNT, kartTO.getPriceDiscount());
        values.put(Mapeamento.PERCENTUAL_DISCOUNT, kartTO.getPercentualDiscount());
        values.put(Mapeamento.QUANTITY, kartTO.getQuantity());
        values.put(Mapeamento.VOUCHER_CODE, kartTO.getVoucherCode());

        return values;

    }

    @Override
    public void fill(Cursor cursor) {

        kartTO = new KartTO();

        kartTO.setPrimaryKey(cursor.getInt(cursor.getColumnIndex(Mapeamento.PRIMARY_KEY)));
        kartTO.setPromotionId(cursor.getInt(cursor.getColumnIndex(Mapeamento.PROMOTION_ID)));
        kartTO.setSellerName(cursor.getString(cursor.getColumnIndex(Mapeamento.SELLER_NAME)));
        kartTO.setDescription(cursor.getString(cursor.getColumnIndex(Mapeamento.DESCRIPTION)));
        kartTO.setBannerUrl(cursor.getString(cursor.getColumnIndex(Mapeamento.BANNER_URL)));
        kartTO.setPrice(cursor.getDouble(cursor.getColumnIndex(Mapeamento.PRICE)));
        kartTO.setPriceDiscount(cursor.getDouble(cursor.getColumnIndex(Mapeamento.PRICE_DISCOUNT)));
        kartTO.setPercentualDiscount(cursor.getInt(cursor.getColumnIndex(Mapeamento.PERCENTUAL_DISCOUNT)));
        kartTO.setQuantity(cursor.getInt(cursor.getColumnIndex(Mapeamento.QUANTITY)));
        kartTO.setVoucherCode(cursor.getString(cursor.getColumnIndex(Mapeamento.VOUCHER_CODE)));

    }

    @Override
    public String getId() {
        return kartTO.getPrimaryKey().toString();
    }

    @Override
    public String getColumnId() {
        return Mapeamento.PRIMARY_KEY;
    }

    @Override
    public String getColumnOrder() {
        return Mapeamento.PRIMARY_KEY;
    }

    public KartTO getKartTO() {
        return kartTO;
    }
}
