package br.com.adscaller.storage;

import android.content.Context;

public class KartDAO extends DAO<KartPO> {

	public KartDAO(Context context) {
		super(context, KartPO.class);
	}

	public KartPO findByPromotion(Integer promotionId) {
		return findFirst(KartPO.Mapeamento.PROMOTION_ID + "=?", String.valueOf(promotionId));
	}

}
