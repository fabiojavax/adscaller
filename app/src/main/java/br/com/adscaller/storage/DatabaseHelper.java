package br.com.adscaller.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String NOME_BANCO = "adscaller.db3";
    private static final int VERSAO = 8;
    private final Context context;

    public DatabaseHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.i("DATABASE", "Criando banco...");

        List<String> createList = new ArrayList<String>();
        createList.add(FavoritePO.Mapeamento.CREATE);
        createList.add(RecentsPO.Mapeamento.CREATE);
        createList.add(BannerPO.Mapeamento.CREATE);
        createList.add(VoucherPO.Mapeamento.CREATE);
        createList.add(CardPO.Mapeamento.CREATE);
        createList.add(KartPO.Mapeamento.CREATE);

        runScript(db, createList);

    }

    public void runScript(SQLiteDatabase db, List<String> lines) {
        try {
            String sqlAcumulado = "";
            for (String sql : lines) {
                sqlAcumulado += sql;

                if (sqlAcumulado.endsWith(";")) {
                    db.execSQL(sqlAcumulado);
                    sqlAcumulado = "";
                }
            }
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error: " + e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.i("DATABASE", "Atualizando banco de " + oldVersion + " para " + newVersion + "...");

        List<String> createList = new ArrayList<String>();

        if (!existsTable(db, FavoritePO.Mapeamento.TABLE)) {
            createList.add(FavoritePO.Mapeamento.CREATE);
        }

        if (!existsTable(db, RecentsPO.Mapeamento.TABLE)) {
            createList.add(RecentsPO.Mapeamento.CREATE);
        }

        if (!existsTable(db, BannerPO.Mapeamento.TABLE)) {
            createList.add(BannerPO.Mapeamento.CREATE);
        }

        if (!existsTable(db, VoucherPO.Mapeamento.TABLE)) {
            createList.add(VoucherPO.Mapeamento.CREATE);
        }
        if (!existsTable(db, CardPO.Mapeamento.TABLE)) {
            createList.add(CardPO.Mapeamento.CREATE);
        }
        if (!existsTable(db, KartPO.Mapeamento.TABLE)) {
            createList.add(KartPO.Mapeamento.CREATE);
        }

        runScript(db, createList);

    }

    private boolean existsColumnInTable(SQLiteDatabase db, String inTable, String columnToCheck) {
        try {

            Cursor mCursor = db.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

            if (mCursor.getColumnIndex(columnToCheck) != -1) {
                return true;
            } else {
                return false;
            }

        } catch (Exception Exp) {
            return false;
        }
    }

    private boolean existsTable(SQLiteDatabase db, String inTable) {
        try {
            Cursor mCursor = db.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);
            return true;
        } catch (Exception Exp) {
            return false;
        }
    }

    private boolean existsDataInTable(SQLiteDatabase db, String inTable) {
        try {

            Cursor mCursor = db.rawQuery("SELECT count(*) total FROM " + inTable + " LIMIT 1", null);

            return mCursor.moveToNext();

        } catch (Exception Exp) {
            return false;
        }
    }


    public static void deleteTables(SQLiteDatabase db, List<String> tables) {
        for (String table : tables) {
            try {
                db.execSQL("DROP TABLE IF EXISTS " + table);
            } catch (Exception e) {
            }
        }
    }

    public static void removeDataFromAllTables(SQLiteDatabase db) {

        List<String> listTables = new LinkedList<>();

        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                listTables.add(c.getString(c.getColumnIndex("name")));
                c.moveToNext();
            }
        }

        for (String tableName : listTables) {
            db.execSQL("delete from "+ tableName);
        }

    }

    public static void removeDataFromTables(SQLiteDatabase db, List<String> tables) {
        for (String tableName : tables) {
            db.execSQL("delete from "+ tableName);
        }
    }

    public static List<String> getTablesList(SQLiteDatabase db) {
        List<String> listTables = new LinkedList<>();


        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                listTables.add(c.getString(c.getColumnIndex("name")));
                c.moveToNext();
            }
        }

        return listTables;
    }
}
