package br.com.adscaller.storage;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.adscaller.transferobject.BannerTO;
import br.com.adscaller.transferobject.PromotionTO;
import br.com.adscaller.transferobject.SellerTO;

@SuppressWarnings("serial")
public class BannerPO extends TransferObject {

    private BannerTO bannerTO;

    public interface Mapeamento {

        String TABLE = "banner";
        String PRIMARY_KEY = "_pk";
        String ID = "bannerId";
        String URL = "bannerUrl";
        String PROMOTION_ID = "promotionId";
        String TITLE = "title";
        String DESCRIPTION = "description";
        String PRICE = "price";
        String PRICE_DISCOUNT = "price_discount";
        String PERCENTUAL_DISCOUNT = "percentual_discount";
        String EXPIRATION = "expiration";
        String SELLER_NAME = "viewSellerName";


        String CREATE = new StringBuilder("create table ").append(TABLE).append(" (")
                .append(PRIMARY_KEY).append(TYPE_PRIMARY_KEY)
                .append(", ").append(ID).append(TYPE_INTEGER)
                .append(", ").append(URL).append(TYPE_TEXT)
                .append(", ").append(PROMOTION_ID).append(TYPE_INTEGER)
                .append(", ").append(TITLE).append(TYPE_TEXT)
                .append(", ").append(DESCRIPTION).append(TYPE_TEXT)
                .append(", ").append(PRICE).append(TYPE_NUMERIC)
                .append(", ").append(PRICE_DISCOUNT).append(TYPE_NUMERIC)
                .append(", ").append(PERCENTUAL_DISCOUNT).append(TYPE_INTEGER)
                .append(", ").append(EXPIRATION).append(TYPE_TEXT)
                .append(", ").append(SELLER_NAME).append(TYPE_TEXT)
                .append(");").toString();

    }

    @Override
    public String getTableName() {
        return Mapeamento.TABLE;
    }

    public BannerPO() {
        super();
    }

    public BannerPO(BannerTO bannerTO) {
        super();
        this.bannerTO = bannerTO;
    }

    @Override
    public ContentValues getMapping() {

        ContentValues values = new ContentValues();

        values.put(Mapeamento.PRIMARY_KEY, bannerTO.getPrimaryKey());
        values.put(Mapeamento.ID, bannerTO.getBannerId());
        values.put(Mapeamento.URL, bannerTO.getBannerUrl());
        values.put(Mapeamento.PROMOTION_ID, bannerTO.getPromotionId());
        values.put(Mapeamento.TITLE, bannerTO.getPromotion().getTitle());
        values.put(Mapeamento.DESCRIPTION, bannerTO.getPromotion().getDescription());
        values.put(Mapeamento.PRICE, bannerTO.getPromotion().getPrice());
        values.put(Mapeamento.PERCENTUAL_DISCOUNT, bannerTO.getPromotion().getPercentualDiscount());
        values.put(Mapeamento.EXPIRATION, bannerTO.getPromotion().getValidUntil());
        values.put(Mapeamento.SELLER_NAME, bannerTO.getPromotion().getSeller().getName());

        return values;

    }

    @Override
    public void fill(Cursor cursor) {

        bannerTO = new BannerTO();

        bannerTO.setPrimaryKey(cursor.getInt(cursor.getColumnIndex(Mapeamento.PRIMARY_KEY)));
        bannerTO.setBannerId(cursor.getInt(cursor.getColumnIndex(Mapeamento.ID)));
        bannerTO.setBannerUrl(cursor.getString(cursor.getColumnIndex(Mapeamento.URL)));
        bannerTO.setPromotionId(cursor.getInt(cursor.getColumnIndex(Mapeamento.PROMOTION_ID)));
        PromotionTO promotionTO = new PromotionTO();
        promotionTO.setPromotionId(bannerTO.getPromotionId());
        promotionTO.setTitle(cursor.getString(cursor.getColumnIndex(Mapeamento.TITLE)));
        promotionTO.setDescription(cursor.getString(cursor.getColumnIndex(Mapeamento.DESCRIPTION)));
        promotionTO.setPrice(cursor.getDouble(cursor.getColumnIndex(Mapeamento.PRICE)));
        promotionTO.setPercentualDiscount(cursor.getInt(cursor.getColumnIndex(Mapeamento.PERCENTUAL_DISCOUNT)));
        promotionTO.setValidUntil(cursor.getString(cursor.getColumnIndex(Mapeamento.EXPIRATION)));
        SellerTO sellerTO = new SellerTO();
        sellerTO.setName(cursor.getString(cursor.getColumnIndex(Mapeamento.SELLER_NAME)));
        promotionTO.setSeller(sellerTO);
        bannerTO.setPromotion(promotionTO);


        ;

    }

    @Override
    public String getId() {
        return bannerTO.getPrimaryKey().toString();
    }

    @Override
    public String getColumnId() {
        return Mapeamento.PRIMARY_KEY;
    }

    @Override
    public String getColumnOrder() {
        return Mapeamento.PRIMARY_KEY;
    }

    public BannerTO getBannerTO() {
        return bannerTO;
    }
}
