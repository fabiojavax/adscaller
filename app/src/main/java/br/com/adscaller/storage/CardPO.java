package br.com.adscaller.storage;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.adscaller.transferobject.CardTO;

@SuppressWarnings("serial")
public class CardPO extends TransferObject {

    private CardTO cardTO;

    public interface Mapeamento {

        String TABLE = "card";
        String PRIMARY_KEY = "_pk";
        String USER_ID = "user_id";
        String NUMBER = "number";
        String REDACTED_NUMBER = "redacted_number";
        String HOLDER_NAME = "holder_name";
        String CVV = "cvv";
        String EXPIRY_MONTH = "expiry_month";
        String EXPIRY_YEAR = "expiry_year";

        String CREATE = new StringBuilder("create table ").append(TABLE).append(" (")
                .append(PRIMARY_KEY).append(TYPE_PRIMARY_KEY)
                .append(", ").append(NUMBER).append(TYPE_TEXT)
                .append(", ").append(USER_ID).append(TYPE_INTEGER)
                .append(", ").append(REDACTED_NUMBER).append(TYPE_TEXT)
                .append(", ").append(HOLDER_NAME).append(TYPE_TEXT)
                .append(", ").append(CVV).append(TYPE_TEXT)
                .append(", ").append(EXPIRY_MONTH).append(TYPE_TEXT)
                .append(", ").append(EXPIRY_YEAR).append(TYPE_TEXT)
                .append(");").toString();

    }

    @Override
    public String getTableName() {
        return Mapeamento.TABLE;
    }

    public CardPO() {
        super();
    }

    public CardPO(CardTO cardTO) {
        super();
        this.cardTO = cardTO;
    }

    @Override
    public ContentValues getMapping() {

        ContentValues values = new ContentValues();

        values.put(Mapeamento.PRIMARY_KEY, cardTO.getPrimaryKey());
        values.put(Mapeamento.USER_ID, cardTO.getUserId());
        values.put(Mapeamento.NUMBER, cardTO.getCardNumber());
        values.put(Mapeamento.REDACTED_NUMBER, cardTO.getRedactedCardNumber());
        values.put(Mapeamento.HOLDER_NAME, cardTO.getCardholderName());
        values.put(Mapeamento.CVV, cardTO.getCvv());
        values.put(Mapeamento.EXPIRY_MONTH, cardTO.getExpiryMonth());
        values.put(Mapeamento.EXPIRY_YEAR, cardTO.getExpiryYear());

        return values;

    }

    @Override
    public void fill(Cursor cursor) {

        cardTO = new CardTO();

        cardTO.setPrimaryKey(cursor.getInt(cursor.getColumnIndex(Mapeamento.PRIMARY_KEY)));
        cardTO.setUserId(cursor.getInt(cursor.getColumnIndex(Mapeamento.USER_ID)));
        cardTO.setCardNumber(cursor.getString(cursor.getColumnIndex(Mapeamento.NUMBER)));
        cardTO.setRedactedCardNumber(cursor.getString(cursor.getColumnIndex(Mapeamento.REDACTED_NUMBER)));
        cardTO.setCardholderName(cursor.getString(cursor.getColumnIndex(Mapeamento.HOLDER_NAME)));
        cardTO.setCvv(cursor.getString(cursor.getColumnIndex(Mapeamento.CVV)));
        cardTO.setExpiryMonth(cursor.getString(cursor.getColumnIndex(Mapeamento.EXPIRY_MONTH)));
        cardTO.setExpiryYear(cursor.getString(cursor.getColumnIndex(Mapeamento.EXPIRY_YEAR)));

    }

    @Override
    public String getId() {
        return cardTO.getPrimaryKey().toString();
    }

    @Override
    public String getColumnId() {
        return Mapeamento.PRIMARY_KEY;
    }

    @Override
    public String getColumnOrder() {
        return Mapeamento.PRIMARY_KEY;
    }

    public CardTO getCardTO() {
        return cardTO;
    }
}
