package br.com.adscaller.storage;

import android.content.Context;

public class BannerDAO extends DAO<BannerPO> {

	public BannerDAO(Context context) {
		super(context, BannerPO.class);
	}

	public BannerPO findById(Integer bannerId) {
		return findFirst(BannerPO.Mapeamento.ID + "=?", String.valueOf(bannerId));
	}

}
