package br.com.adscaller.storage;

import android.content.Context;

public class FavoriteDAO extends DAO<FavoritePO> {

	public FavoriteDAO(Context context) {
		super(context, FavoritePO.class);
	}

	public FavoritePO findByPhone(String phone) {
		return findFirst(FavoritePO.Mapeamento.PHONE + "=?", new String[] { phone });
	}

}
