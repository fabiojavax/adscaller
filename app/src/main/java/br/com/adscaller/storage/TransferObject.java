package br.com.adscaller.storage;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class TransferObject implements Serializable {

	public static final String TYPE_PRIMARY_KEY = " integer primary key autoincrement ";
	public static final String TYPE_TEXT = " text ";
	public static final String TYPE_NUMERIC = " numeric ";
	public static final String TYPE_INTEGER = " integer ";
	public static final String TYPE_BOOLEAN = " boolean ";

	public abstract String getTableName();

	public abstract ContentValues getMapping();

	public abstract void fill(Cursor cursor);

	public abstract String getId();

	public abstract String getColumnId();

	public String getColumnOrder() {
		return null;
	}
}
