package br.com.adscaller.storage;

import android.content.Context;

public class RecentsDAO extends DAO<RecentsPO> {

	public RecentsDAO(Context context) {
		super(context, RecentsPO.class);
	}

	public RecentsPO findByPhone(String phone) {
		return findFirst(RecentsPO.Mapeamento.PHONE + "=?", new String[] { phone });
	}

}
