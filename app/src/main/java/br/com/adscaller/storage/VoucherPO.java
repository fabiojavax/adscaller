package br.com.adscaller.storage;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.adscaller.transferobject.VoucherTO;

@SuppressWarnings("serial")
public class VoucherPO extends TransferObject {

    private VoucherTO voucherTO;

    public interface Mapeamento {

        String TABLE = "voucher";
        String PRIMARY_KEY = "_pk";
        String PROMOTION_ID = "promotion_id";
        String PROMOTION = "promotion";
        String CODE = "code";
        String EXPIRATION = "expiration";

        String CREATE = new StringBuilder("create table ").append(TABLE).append(" (")
                .append(PRIMARY_KEY).append(TYPE_PRIMARY_KEY)
                .append(", ").append(PROMOTION_ID).append(TYPE_INTEGER)
                .append(", ").append(PROMOTION).append(TYPE_TEXT)
                .append(", ").append(CODE).append(TYPE_TEXT)
                .append(", ").append(EXPIRATION).append(TYPE_TEXT)
                .append(");").toString();

    }

    @Override
    public String getTableName() {
        return Mapeamento.TABLE;
    }

    public VoucherPO() {
        super();
    }

    public VoucherPO(VoucherTO voucherTO) {
        super();
        this.voucherTO = voucherTO;
    }

    @Override
    public ContentValues getMapping() {

        ContentValues values = new ContentValues();

        values.put(Mapeamento.PRIMARY_KEY, voucherTO.getPrimaryKey());
        values.put(Mapeamento.PROMOTION_ID, voucherTO.getPromotionId());
        values.put(Mapeamento.PROMOTION, voucherTO.getPromotion());
        values.put(Mapeamento.CODE, voucherTO.getCode());
        values.put(Mapeamento.EXPIRATION, voucherTO.getExpiration());

        return values;

    }

    @Override
    public void fill(Cursor cursor) {

        voucherTO = new VoucherTO();

        voucherTO.setPrimaryKey(cursor.getInt(cursor.getColumnIndex(Mapeamento.PRIMARY_KEY)));
        voucherTO.setPromotionId(cursor.getInt(cursor.getColumnIndex(Mapeamento.PROMOTION_ID)));
        voucherTO.setPromotion(cursor.getString(cursor.getColumnIndex(Mapeamento.PROMOTION)));
        voucherTO.setCode(cursor.getString(cursor.getColumnIndex(Mapeamento.CODE)));
        voucherTO.setExpiration(cursor.getString(cursor.getColumnIndex(Mapeamento.EXPIRATION)));

    }

    @Override
    public String getId() {
        return voucherTO.getPrimaryKey().toString();
    }

    @Override
    public String getColumnId() {
        return Mapeamento.PRIMARY_KEY;
    }

    @Override
    public String getColumnOrder() {
        return Mapeamento.PRIMARY_KEY;
    }

    public VoucherTO getVoucherTO() {
        return voucherTO;
    }
}
