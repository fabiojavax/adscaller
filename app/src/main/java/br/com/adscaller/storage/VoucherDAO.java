package br.com.adscaller.storage;

import android.content.Context;

public class VoucherDAO extends DAO<VoucherPO> {

	public VoucherDAO(Context context) {
		super(context, VoucherPO.class);
	}

	public VoucherPO findByPromotionId(Integer promotionId) {
		return findFirst(VoucherPO.Mapeamento.PROMOTION_ID + "=?", String.valueOf(promotionId));
	}

}
