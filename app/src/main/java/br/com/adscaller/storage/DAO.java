package br.com.adscaller.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public abstract class DAO<T extends TransferObject> {

    public final static String SIM = "1";
    public final static String NAO = "0";

    private final T exemplo;
    private final Class<T> clazz;
    protected Context context;

    public DAO(Context context, Class<T> clazz) {
        this.context = context;
        this.clazz = clazz;
        try {
            exemplo = clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public long create(T object) {
        long retorno = -1;
        SQLiteDatabase db = null;
        try {
            db = DatabaseManager.getInstance(context).openDatabase();
            retorno = db.insert(object.getTableName(), null, object.getMapping());
            return retorno;
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

    /**
     * Atualiza usando o mapping padrão. Ou seja, todos os atributos do objeto.
     * @param object
     */
    public void update(T object) {
        update(object, object.getMapping());
    }

    /**
     * Atualiza no objeto passado somente os campos definidos no ContentValues
     * @param object
     * @param contentValues
     */
    public void update(T object, ContentValues contentValues) {
        SQLiteDatabase db = null;
        try {
            db = DatabaseManager.getInstance(context).openDatabase();
            db.update(exemplo.getTableName(), contentValues, object.getColumnId() + "=?", new String[] { object.getId() });
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

    public void remove(T object) {
        SQLiteDatabase db = null;
        try {
            if (object.getId() != null) {
                db = DatabaseManager.getInstance(context).openDatabase();
                db.delete(exemplo.getTableName(), object.getColumnId() + "=?", new String[] { object.getId() });
            }
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

    public void removeAll() {
        SQLiteDatabase db = null;
        try {
            db = DatabaseManager.getInstance(context).openDatabase();
            db.delete(exemplo.getTableName(), null, null);
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

    public List<T> findAll() {
        return findBy(null);
    }

    /**
     *
     * @param where Precisa ter o =?. Exemplo: "nome_da_coluna=?"
     * @param params
     * @return
     */
    protected List<T> findBy(String where, String... params) {
        return findBy(null, where, exemplo.getColumnOrder(), params);
    }

    /**
     *
     * @param where Precisa ter o =?. Exemplo: "nome_da_coluna=?"
     * @param params
     * @return
     */
    protected List<T> findBy(String[] columns, String where, String... params) {
        List<T> resultado = new ArrayList<T>();
        SQLiteDatabase db = null;

        try {
            db = DatabaseManager.getInstance(context).openDatabase();
            Cursor cursor = db.query(exemplo.getTableName(), columns, where, params, null, null, exemplo.getColumnOrder());
            while (cursor.moveToNext()) {
                T novo = clazz.newInstance();
                novo.fill(cursor);
                resultado.add(novo);
            }
            if (cursor != null) {
                cursor.close();
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
        return resultado;
    }

    /**
     *
     * @param where Precisa ter o =?. Exemplo: "nome_da_coluna=?"
     * @param params
     * @return
     */
    protected List<T> findBy(String[] columns, String where, String orderBy, String... params) {
        List<T> resultado = new ArrayList<T>();
        SQLiteDatabase db = null;

        try {
            db = DatabaseManager.getInstance(context).openDatabase();
            Cursor cursor = db.query(exemplo.getTableName(), columns, where, params, null, null, orderBy);
            while (cursor.moveToNext()) {
                T novo = clazz.newInstance();
                novo.fill(cursor);
                resultado.add(novo);
            }
            if (cursor != null) {
                cursor.close();
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
        return resultado;
    }

    public T findByPK(String id) {
        SQLiteDatabase db = null;

        try {
            T resultado = null;
            T exemplo = clazz.newInstance();
            db = DatabaseManager.getInstance(context).openDatabase();
            Cursor cursor = db.query(exemplo.getTableName(), null,
                    exemplo.getColumnId() + "=?", new String[] { id }, null, null, null,
                    null);
            if (cursor.moveToNext()) {
                resultado = clazz.newInstance();
                resultado.fill(cursor);
            }
            if (cursor != null) {
                cursor.close();
            }
            return resultado;
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

    public void removeByPK(T item) {
        SQLiteDatabase db = null;

        try {
            db = DatabaseManager.getInstance(context).openDatabase();
            db.delete(exemplo.getTableName(), item.getColumnId() + "=?", new String[] { item.getId() });
        } finally {
            if (db != null && db.isOpen()) {
                DatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

    protected T findFirst(String where, String... params) {
        List<T> lista = findBy(where, params);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }
}
