package br.com.adscaller.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.adscaller.UserPrefs;

public class CardDAO extends DAO<CardPO> {

	public CardDAO(Context context) {
		super(context, CardPO.class);
	}

	public CardPO findByNumber(String cardNumber) {
		return findFirst(CardPO.Mapeamento.NUMBER + "=?", cardNumber);
	}

	public List<CardPO> findAll() {
		StringBuilder sql = new StringBuilder();

		sql.append(" select * from ").append(CardPO.Mapeamento.TABLE).append(" where user_id = ")
				.append(UserPrefs.getUser(context).getId());

		List<CardPO> resultado = new ArrayList<>();
		SQLiteDatabase db = null;
		try {
			db = DatabaseManager.getInstance(context).openDatabase();
			Cursor cursor = db.rawQuery(sql.toString(), null);
			while (cursor.moveToNext()) {
				CardPO cardPO = new CardPO();
				cardPO.fill(cursor);
				resultado.add(cardPO);
			}
			cursor.close();
		} finally {
			if (db != null && db.isOpen()) {
				DatabaseManager.getInstance(context).closeDatabase();
			}
		}
		return resultado;
	}

}
