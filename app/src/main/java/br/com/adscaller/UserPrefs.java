/*
    Esse código-fonte pertence à ORUS ON-LINE LTDA, CNPJ 14.860.302/0001-44
    e não pode ser copiado, consultado ou utilizado, para fins pessoais ou de terceiros,
    sem a expressa autorização por escrito da mesma. A não observância desses termos implicará nas
    penalidades previstas na LEI Nº 9.609/98
 */

package br.com.adscaller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import br.com.adscaller.transferobject.UserTO;


public class UserPrefs {

    private static final String PREF_LOGIN = "pref_login";
    private static final String PREF_ID = "pref_id";
    private static final String PREF_TOKEN = "pref_token";
    private static final String PREF_NAME = "pref_name";
    private static final String PREF_EMAIL = "pref_email";
    private static final String PREF_PHONE = "pref_phone";
    private static final String PREF_BIRTHDATE = "pref_birthdate";
    private static final String PREF_AVATAR = "pref_avatar";
    private static final String PREF_SELECTED_CARD = "pref_selected_card";
    private static final String PREF_SELECTED_CATEGORY = "pref_selected_category";


    public static UserTO getUser(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String login = prefs.getString(PREF_LOGIN, null);
        if (login == null) {
            return new UserTO();
        }

        UserTO userTO = new UserTO();

        userTO.setLogin(login);
        userTO.setId(prefs.getInt(PREF_ID, 0));
        userTO.setToken(prefs.getString(PREF_TOKEN, null));
        userTO.setName(prefs.getString(PREF_NAME, null));
        userTO.setEmail(prefs.getString(PREF_EMAIL, null));
        userTO.setPhone(prefs.getString(PREF_PHONE, null));
        userTO.setBirthDate(prefs.getString(PREF_BIRTHDATE, null));
        userTO.setAvatar(prefs.getString(PREF_AVATAR, null));
        userTO.setSelectedCard(prefs.getInt(PREF_SELECTED_CARD, 0));
        userTO.setSelectedCategory(prefs.getInt(PREF_SELECTED_CATEGORY, 0));

        return userTO;
    }

    public static void setUser(Context context, UserTO userTO) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();

        edit.putInt(PREF_ID, userTO.getId());
        edit.putString(PREF_LOGIN, userTO.getLogin());
        edit.putString(PREF_TOKEN, userTO.getToken());
        edit.putString(PREF_NAME, userTO.getName());
        edit.putString(PREF_EMAIL, userTO.getEmail());
        edit.putString(PREF_PHONE, userTO.getPhone());
        edit.putString(PREF_BIRTHDATE, userTO.getBirthDate());
        edit.putString(PREF_AVATAR, userTO.getAvatar());
        edit.putInt(PREF_SELECTED_CARD, userTO.getSelectedCard());
        edit.putInt(PREF_SELECTED_CATEGORY, userTO.getSelectedCategory());

        edit.apply();
    }

    public static void clear(Context context) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();

        edit.remove(PREF_ID);
        edit.remove(PREF_LOGIN);
        edit.remove(PREF_TOKEN);
        edit.remove(PREF_NAME);
        edit.remove(PREF_EMAIL);
        edit.remove(PREF_PHONE);
        edit.remove(PREF_BIRTHDATE);
        edit.remove(PREF_AVATAR);
        edit.remove(PREF_SELECTED_CARD);
        edit.remove(PREF_SELECTED_CATEGORY);

        edit.apply();

    }


}
