/*
    Esse código-fonte pertence à ORUS ON-LINE LTDA, CNPJ 14.860.302/0001-44
    e não pode ser copiado, consultado ou utilizado, para fins pessoais ou de terceiros,
    sem a expressa autorização por escrito da mesma. A não observância desses termos implicará nas
    penalidades previstas na LEI Nº 9.609/98
 */

package br.com.adscaller.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.activity.IntroActivity;
import br.com.adscaller.activity.MainActivity;
import br.com.adscaller.transferobject.LoginRequest;
import br.com.adscaller.transferobject.LoginResponse;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.adscaller.Constants.CONTENT_TYPE_JSON;

public class LoginTask extends AsyncTask<Void, String, Boolean> {

    ProgressDialog progressDialog;

    Activity activity;

    LoginResponse loginResponse;

    String login;
    String senha;

    public LoginTask(Activity activity, String login, String senha) {
        this.login = login;
        this.senha = senha;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(activity.getString(R.string.logging));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {

            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS).build();

            Gson gson = new GsonBuilder().create();

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setLogin(login);
            loginRequest.setPassword(senha);

            RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(loginRequest));
            Request request = new Request.Builder()
                    .url("http://adscallerapi.azurewebsites.net/api/Auth/login")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody != null) {

                String apiResponse = responseBody.string();

                if (StringHelper.isNotBlank(apiResponse)) {

                    loginResponse = gson.fromJson(apiResponse, LoginResponse.class);

                    return true;

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            LogHelper.log(e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (!isCancelled()) {

            if (!success || loginResponse == null) {

                AlertAndroid.showMessageDialog(activity, R.string.service_error);

            } else {

                if (!loginResponse.isAuthenticated()) {

                    if (StringHelper.isNotBlank(loginResponse.getMessage())) {

                        AlertAndroid.showMessageDialog(activity, loginResponse.getMessage());

                    } else {

                        AlertAndroid.showMessageDialog(activity, R.string.login_invalido);

                    }

                } else {

                    UserTO userTO = new UserTO();
                    userTO.setId(loginResponse.getId());
                    userTO.setLogin(login);
                    userTO.setToken(loginResponse.getAccessToken());
                    userTO.setName(loginResponse.getName());
                    userTO.setEmail(loginResponse.getEmail());

                    UserPrefs.setUser(activity, userTO);

                    Intent i = new Intent(activity, MainActivity.class);
                    activity.startActivity(i);

                    activity.finish();

                }

            }

        }

    }

}
