/*
    Esse código-fonte pertence à ORUS ON-LINE LTDA, CNPJ 14.860.302/0001-44
    e não pode ser copiado, consultado ou utilizado, para fins pessoais ou de terceiros,
    sem a expressa autorização por escrito da mesma. A não observância desses termos implicará nas
    penalidades previstas na LEI Nº 9.609/98
 */

package br.com.adscaller.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.transferobject.CadastroResponse;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class GetUserTask extends AsyncTask<Void, String, Boolean> {

    ProgressDialog progressDialog;

    Activity activity;

    CadastroResponse cadastroResponse;

    public GetUserTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(activity.getString(R.string.getting_user_data));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder().create();

            UserTO userTO = UserPrefs.getUser(activity);

            HttpUrl httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("adscallerapi.azurewebsites.net")
                    .addPathSegment("api/User/" + userTO.getToken())
                    .addQueryParameter("email", userTO.getEmail())
                    .build();

            Request request = new Request.Builder()
                    .url(httpUrl)
                    .get()
                    .build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody != null) {

                String apiResponse = responseBody.string();

                System.out.println("### RESPONSE: " + apiResponse);

                if (StringHelper.isNotBlank(apiResponse)) {

                    cadastroResponse = gson.fromJson(apiResponse, CadastroResponse.class);

                    return true;

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            LogHelper.log(e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (!isCancelled()) {

            if (success && cadastroResponse != null) {

                UserTO userTO = UserPrefs.getUser(activity);
                userTO.setId(cadastroResponse.getUserId());
                userTO.setLogin(cadastroResponse.getEmail());
                userTO.setName(cadastroResponse.getName());
                userTO.setEmail(cadastroResponse.getEmail());
                userTO.setPhone(cadastroResponse.getPhoneNumber());
                try {
                    String data = cadastroResponse.getBirthDate().split("T")[0];
                    Date date = new SimpleDateFormat("yyyy-MM-dd",
                            Locale.getDefault()).parse(data);
                    String newDate = new SimpleDateFormat("dd-MM-yyyy",
                            Locale.getDefault()).format(date);
                    userTO.setBirthDate(newDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                userTO.setGender(cadastroResponse.getGender());

                UserPrefs.setUser(activity, userTO);

                Intent refreshFavoritesBroadcast = new Intent("fill.user.data");
                LocalBroadcastManager.getInstance(activity).sendBroadcast(refreshFavoritesBroadcast);

            }

        }

    }

}
