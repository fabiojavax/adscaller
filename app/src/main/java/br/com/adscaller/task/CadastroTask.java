/*
    Esse código-fonte pertence à ORUS ON-LINE LTDA, CNPJ 14.860.302/0001-44
    e não pode ser copiado, consultado ou utilizado, para fins pessoais ou de terceiros,
    sem a expressa autorização por escrito da mesma. A não observância desses termos implicará nas
    penalidades previstas na LEI Nº 9.609/98
 */

package br.com.adscaller.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.activity.IntroActivity;
import br.com.adscaller.activity.LoginActivity;
import br.com.adscaller.activity.MainActivity;
import br.com.adscaller.transferobject.CadastroRequest;
import br.com.adscaller.transferobject.CadastroResponse;
import br.com.adscaller.transferobject.LoginRequest;
import br.com.adscaller.transferobject.LoginResponse;
import br.com.adscaller.transferobject.UserTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.adscaller.Constants.CONTENT_TYPE_JSON;

public class CadastroTask extends AsyncTask<Void, String, Boolean> {

    ProgressDialog progressDialog;

    Activity activity;

    CadastroResponse cadastroResponse;

    private String nome;
    private String senha;
    private String email;
    private String telefone;
    private String nascimento;
    private String sexo;
    private String imageUrl;

    public CadastroTask(Activity activity, String nome, String senha, String email,
                        String telefone, String nascimento, String sexo, String imageUrl) {
        this.activity = activity;
        this.nome = nome;
        this.senha = senha;
        this.email = email;
        this.telefone = telefone;
        this.nascimento = nascimento;
        this.sexo = sexo;
        this.imageUrl = imageUrl;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(activity.getString(R.string.subscribing));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder().create();

            CadastroRequest cadastroRequest = new CadastroRequest();
            cadastroRequest.setName(nome);
            cadastroRequest.setPassword(senha);
            cadastroRequest.setEmail(email);
            cadastroRequest.setPhoneNumber(telefone);
            Date date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(nascimento);
            cadastroRequest.setBirthDate(new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault()).format(date));
            cadastroRequest.setGender(Integer.parseInt(sexo));
            cadastroRequest.setPhotoUrl(imageUrl);

            RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(cadastroRequest));
            Request request = new Request.Builder()
                    .url("http://adscallerapi.azurewebsites.net/api/User")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            System.out.println("### REQUEST: " + gson.toJson(cadastroRequest));

            if (responseBody != null) {

                String apiResponse = responseBody.string();

                System.out.println("### RESPONSE: " + apiResponse);

                if (StringHelper.isNotBlank(apiResponse)) {

                    cadastroResponse = gson.fromJson(apiResponse, CadastroResponse.class);

                    return true;

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            LogHelper.log(e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (!isCancelled()) {

            if (!success || cadastroResponse == null) {

                AlertAndroid.showMessageDialog(activity, R.string.service_error);

            } else {

                if (cadastroResponse.getUserId() == 0) {

                    AlertAndroid.showMessageDialog(activity, R.string.erro_cadastro);

                } else {

                    /*
                    UserTO userTO = UserPrefs.getUser(activity);
                    userTO.setId(cadastroResponse.getUserId());
                    userTO.setLogin(cadastroResponse.getEmail());
                    userTO.setToken(cadastroResponse.getAccessToken());
                    userTO.setName(cadastroResponse.getName());
                    userTO.setEmail(cadastroResponse.getEmail());
                    userTO.setPhone(cadastroResponse.getPhoneNumber());
                    userTO.setBirthDate(cadastroResponse.getBirthDate());
                    userTO.setGender(cadastroResponse.getGender());

                    if (StringHelper.isNotBlank(imageUrl)) {
                        userTO.setAvatar(imageUrl);
                    }

                    UserPrefs.setUser(activity, userTO);*/

                    Intent intent = new Intent(activity, LoginActivity.class);
                    intent.putExtra("fromCadastro", true);
                    activity.startActivity(intent);

                    activity.finish();

                }

            }

        }

    }

}
