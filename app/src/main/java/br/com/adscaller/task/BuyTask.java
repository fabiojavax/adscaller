/*
    Esse código-fonte pertence à ORUS ON-LINE LTDA, CNPJ 14.860.302/0001-44
    e não pode ser copiado, consultado ou utilizado, para fins pessoais ou de terceiros,
    sem a expressa autorização por escrito da mesma. A não observância desses termos implicará nas
    penalidades previstas na LEI Nº 9.609/98
 */

package br.com.adscaller.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.adscaller.R;
import br.com.adscaller.UserPrefs;
import br.com.adscaller.activity.OrderActivity;
import br.com.adscaller.storage.CardDAO;
import br.com.adscaller.storage.CardPO;
import br.com.adscaller.storage.KartDAO;
import br.com.adscaller.storage.KartPO;
import br.com.adscaller.transferobject.BuyItemTO;
import br.com.adscaller.transferobject.BuyRequest;
import br.com.adscaller.transferobject.BuyResponse;
import br.com.adscaller.transferobject.CardTO;
import br.com.adscaller.transferobject.CreditCardTO;
import br.com.adscaller.transferobject.KartTO;
import br.com.adscaller.util.AlertAndroid;
import br.com.adscaller.util.LogHelper;
import br.com.adscaller.util.StringHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.adscaller.Constants.CONTENT_TYPE_JSON;

public class BuyTask extends AsyncTask<Void, String, Boolean> {

    private ProgressDialog progressDialog;

    Activity activity;

    private BuyRequest buyRequest = new BuyRequest();
    private BuyResponse buyResponse;

    public BuyTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(activity.getString(R.string.finalizing_buy));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {

            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS).build();

            Gson gson = new GsonBuilder().create();

            buyRequest.setVoucher(null);
            buyRequest.setUserId(UserPrefs.getUser(activity).getId());

            double total = 0.0;

            CardTO selectedCardTO = new CardTO();
            List<CardPO> cardPOList = new CardDAO(activity).findAll();
            for (CardPO cardPO: cardPOList) {
                CardTO cardTO = cardPO.getCardTO();
                if (cardTO.getPrimaryKey().equals(UserPrefs.getUser(activity).getSelectedCard())) {
                    selectedCardTO = cardTO;
                }
            }

            CreditCardTO creditCardTO = new CreditCardTO();
            creditCardTO.setCreditCardNumber(selectedCardTO.getCardNumber());
            if (Integer.parseInt(selectedCardTO.getExpiryMonth()) < 10) {
                creditCardTO.setExpireMonth("0" + selectedCardTO.getExpiryMonth());
            } else {
                creditCardTO.setExpireMonth(selectedCardTO.getExpiryMonth());
            }
            creditCardTO.setExpireYear(selectedCardTO.getExpiryYear());
            creditCardTO.setHolderName(selectedCardTO.getCardholderName());
            creditCardTO.setSecurityCode(Integer.parseInt(selectedCardTO.getCvv()));

            buyRequest.setCreditCard(creditCardTO);

            List<BuyItemTO> buyItems = new ArrayList<>();
            List<KartPO> kartPOList = new KartDAO(activity).findAll();
            for (KartPO kartPO: kartPOList) {
                KartTO kartTO = kartPO.getKartTO();
                total += kartTO.getPriceDiscount();
                BuyItemTO buyItemTO = new BuyItemTO();
                buyItemTO.setPromotionId(kartTO.getPromotionId());
                buyItemTO.setQtd(kartTO.getQuantity());
                buyItemTO.setVoucherId(0);
                buyItems.add(buyItemTO);
            }

            buyRequest.setOrderItems(buyItems);

            buyRequest.setPrice(total);

            String url = "http://adscallerapi.azurewebsites.net/api/Order/buy/" +
                    UserPrefs.getUser(activity).getToken();

            RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, gson.toJson(buyRequest));
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            LogHelper.log("BuyTask", url);
            LogHelper.log("BuyTask", gson.toJson(buyRequest));

            if (responseBody != null) {

                String apiResponse = responseBody.string();

                LogHelper.log("BuyTask", "Api Response: " + apiResponse);

                if (StringHelper.isNotBlank(apiResponse)) {

                    buyResponse = gson.fromJson(apiResponse, BuyResponse.class);

                    return true;

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            LogHelper.log(e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (!isCancelled()) {

            if (!success || buyResponse == null) {

                AlertAndroid.showMessageDialog(activity, R.string.service_error);

            } else {

                if ("paid".equalsIgnoreCase(buyResponse.getStatus())) {

                    Intent refreshFavoritesBroadcast = new Intent("close.activity.promotion");
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(refreshFavoritesBroadcast);

                    new KartDAO(activity).removeAll();

                    View messageView = activity.getLayoutInflater().inflate(R.layout.dialog_success,
                            null, false);

                    View viewOk = messageView.findViewById(R.id.view_ok);
                    viewOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activity.finish();
                        }
                    });

                    View viewMyOrders = messageView.findViewById(R.id.view_my_orders);
                    viewMyOrders.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(activity, OrderActivity.class);
                            activity.startActivity(intent);
                            activity.finish();
                        }
                    });

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setView(messageView);
                    builder.create();
                    builder.show();

                } else {

                    AlertAndroid.showMessageDialog(activity, buyResponse.getMessage());

                }

            }

        }

    }

}
