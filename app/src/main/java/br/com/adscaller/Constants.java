package br.com.adscaller;

import okhttp3.MediaType;

public class Constants {

    public static final MediaType CONTENT_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    public static final String FILE_PROVIDER = "com.adscaller.fileprovider";

    public static String AWS_ROOT = "https://s3-sa-east-1.amazonaws.com/youback/";

}
